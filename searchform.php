<form class="search-form" data-flex action="/" method="get">
    <div class="form">
        <input class="js-search-input" type="text" name="s" id="search" placeholder="<?= __('Type', 'search') ?>"/>
        <button class type="submit"><?= __('Search', 'search') ?></button>     
    </div>
</form>