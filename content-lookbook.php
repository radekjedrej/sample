<?php
/**
 * The template used for displaying all projects in template-projects.php
 *
 * @package storefront-child
 */

$lookbook_image = get_field("lookbook_image") ? get_field("lookbook_image") : "";
$lookbook_product_name = get_field("lookbook_product_name") ? get_field("lookbook_product_name") : "";        
$lookbook_collection_name = get_field("lookbook_collection_name") ? get_field("lookbook_collection_name") : "";        

?>

<div class="projects__box projects__box--<?= $template_args['count']; ?>">

    <div class="project__content">

        <div class="projects__media">
            <img class="projects__img" src="<?= $lookbook_image['url'] ?>" alt="<?= $lookbook_image['alt'] ?>">
            <div class="projects__caption">
                <div class="projects__info projects__info--absolute" data-flex="row keep center justify">
                    <div class="projects__title">
                        <h3 class="text-white section-title"><?= $lookbook_product_name; ?></h3>
                    </div>

                    <div class="projects__collection">
                        <p class="text-white"><?= $lookbook_collection_name; ?></p>
                    </div>

                </div>
            </div>
        </div>

        <div class="projects__details projects__details--no-border"></div>

    </div>
</div> 