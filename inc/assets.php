<?php 

// Enqueue header scripts
function load_header_scripts()
{
    wp_register_script('roomvo', 'https://www.roomvo.com/static/scripts/b2b/fuzion.js', array(),false,false);
    wp_enqueue_script('roomvo');
}

function fuzion_enqueue_script( $name, $prefix = 'fuzion', $min = true )
{
    $ext = $min ? 'min.js' : 'js';

    wp_register_script("{$prefix}-{$name}", get_stylesheet_directory_uri() . "/dist/{$name}.{$ext}", array(), false, true);
    wp_enqueue_script("{$prefix}-{$name}");
}

// Enqueue conditional scripts
function load_conditional_scripts()
{
    fuzion_enqueue_script('front');

    wp_register_script('mailchimp', '//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js', array(), false, true);
    wp_enqueue_script('mailchimp');

    wp_add_inline_script('mailchimp', "(function(\$) {window.fnames = new Array();window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var \$mcj = jQuery.noConflict(true);
    document.querySelector('.response.fuzion-title').onclick = function() { this.style.display = 'none' }" );

    if ( is_singular('collections') ) 
    {
        fuzion_enqueue_script('collection');
    }

    if ( is_page('resources') ) 
    {
        fuzion_enqueue_script('resources');
    }

    if ( is_page('journal') ) 
    {
        fuzion_enqueue_script('blog');
    }

    if ( is_page('care-maintenance') ) 
    {
        fuzion_enqueue_script('careMaintenance');
    }

    if (/* is_product_category() || */is_woocommerce() ) 
    {
        fuzion_enqueue_script('product');
    }

    if ( is_home() || is_single() && 'post' == get_post_type() )
    {
        fuzion_enqueue_script('socialShare');
    }

}

// Enqueue styles
function load_styles()
{
    wp_register_style('fuzion-fonts', 'https://use.typekit.net/fyo3bon.css', null);
    wp_enqueue_style('fuzion-fonts');

    $styles = IS_DEV 
        ? 'front.css' 
        : 'front.min.css'
    ;

    wp_register_style('fuzion-styles', get_stylesheet_directory_uri() . "/dist/{$styles}", array(), '1.0', 'all');
    wp_enqueue_style('fuzion-styles');
}

// Favicons
function fuzion_favicon() 
{ ?>
    <link rel="apple-touch-icon" sizes="57x57" href="/wp-content/themes/storefront-child/src/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/wp-content/themes/storefront-child/src/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/wp-content/themes/storefront-child/src/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/wp-content/themes/storefront-child/src/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/wp-content/themes/storefront-child/src/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/wp-content/themes/storefront-child/src/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/wp-content/themes/storefront-child/src/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/wp-content/themes/storefront-child/src/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/wp-content/themes/storefront-child/src/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/wp-content/themes/storefront-child/src/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/wp-content/themes/storefront-child/src/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/wp-content/themes/storefront-child/src/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/wp-content/themes/storefront-child/src/favicon/favicon-16x16.png">
    <link rel="manifest" href="/wp-content/themes/storefront-child/src/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/wp-content/themes/storefront-child/src/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
<?php }

add_filter( 'wp_enqueue_scripts', function() {
    $styles = [
        'storefront-style',
        'storefront-fonts',
        'storefront-icons',
        'chosen-drop-down',
        'modernizr',
        'flexslider',
        'photoswipe'
    ];

    foreach ( $styles as $style ) {
        wp_dequeue_style( $style );
        wp_deregister_style( $style );
    }
}, 20);

add_filter( 'script_loader_tag', function ( $tag, $handle ) {
    switch ( $handle ) {
        case 'roomvo':
            // case 'mailchimp':
            // return str_replace( ' src', ' defer src', $tag ); // defer the script
            return str_replace( ' src', ' async src', $tag ); // OR async the script
        default: 
            return $tag;
    }
}, 10, 2 );

// Enqueue theme assets
add_action('init', 'load_header_scripts');
add_action('wp_head', 'fuzion_favicon');
add_action('wp_enqueue_scripts', 'load_styles'); 
add_action('get_footer', 'load_conditional_scripts');