<?php 

// though this is already in theme.php, it needs to be here also, otherwise the other languages will load the main language menu (canada eng). For some reason, this solution works

$siteUrl = $_SERVER['HTTP_HOST'];
if (strpos($siteUrl, 'professional.') !== false) {
    $mainMenu = get_field('main_menu_professional', 'options');
    $secondaryMenu = get_field('secondary_menu_professional', 'options');
}
else {
    $mainMenu = get_field('main_menu_homeowner', 'options');
    $secondaryMenu = get_field('secondary_menu_homeowner', 'options');

}

$menus = array(
    'main' => $mainMenu,
    'pages' => $secondaryMenu,
    'helper' => get_field('footer_menu', 'options'),
    'social' => get_field('social_menu', 'options'),
);

?>