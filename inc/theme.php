<?php

function bg_cover( $img ) 
{
    $size = wp_is_mobile() ? 'half' : 'hd';
    $src = wp_get_attachment_image_src( $img['id'], $size )[0];
    return "background: var(--c-brown) url({$src}) center / cover";
}

function first_strip( $el, $div = '-' ) 
{
    $el = explode($div, $el);
    return array_shift($el);
}

// Handle layout builders
function fuzion_layout( $name = 'builder_banners', $dir = 'parts' ) 
{
    if ( have_rows( $name ) ) :
        while ( have_rows( $name ) ) : the_row();
    
            $builder = str_replace(
                '_', '-', get_row_layout()
            );
    
            get_template_part( "{$dir}/{$builder}" );
    
        endwhile;
    endif;
}

function has_slider( $post, $builder = 'builder_banners' ) {
    $sliders = ['banner', 'banner_smaller'];
    $hasSlider = false;

    if ( have_rows($builder, $post->ID) ) :
        while ( have_rows($builder, $post->ID) ) : 
            the_row();
            
        if ( strpos(get_row_layout(), $sliders) !== false )
            $hasSlider = true;

        endwhile;
    endif;

    return $hasSlider;	
}

// Handle header class
function fuzion_header_class( $post ) 
{
    $is_home = is_front_page() || strpos(strtolower(get_the_title()), 'homepage') !== false;
    $is_cover = (is_product_category()) && has_slider( $post );
    $is_search = is_search() || strpos(get_the_title(), 'Contact') !== false;

    $name = 'fuzion-header';

    if ( is_404() || $is_home || $is_cover )
        $name = 'fuzion-header fuzion-header--white';

    if ( !is_product() && get_field('support_items') )
        $name = 'fuzion-header fuzion-header--mix';

    if ( $is_search )
        $name = 'fuzion-header';

	echo $name;
}

// Handle CTA buttons
function fuzion_cta_link( $href, $text = 'Learn More', $fill = true, $modifier = '' )
{
    if ( ! $href ) $href = '/' ?>
    <a class="btn--cta<?= $modifier ? ' '.$modifier.'' : '' ?>" href="<?= $href ?>" data-flex="row keep center<?php if ($fill) echo ' justify' ?>">
        <span><?= __($text, 'fuzion') ?></span>
        <?php include get_icons_directory('b-next.svg') ?>
    </a>
    <?php
}

// Handle conditional text paragraph
function fuzion_conditional_text( $name ) 
{
    echo $name ? "<p>{$name}</p>" : '';
}

// Handle accessibility text
function fuzion_reveal_button()
{ 
    ?><div class="sr-only sr-only-focusable"><?= __('Reveal content', 'fuzion') ?></div><?php
}

// Handle site(side)-links
function fuzion_site_links( $links, $header = '', $class = 'site-links--aside' ) 
{ ?>
    <ul class="site-links<?= ' ' . $class ?> list list--block">
    <?php if ($header) echo "<li class='section-caption' style='--border: 0'>{$header}</li>" ?>
    <?php foreach ($links as $item) : ?>
        <li><?php if ($item['type']=="link") {
            echo "<a class='site-links__item' href='{$item['href']}'>{$item['title']}</a>";
        }
        elseif ($item['type']=="download") {
            echo "<a class='site-links__item' href='{$item['file']['url']}' download>{$item['title']}</a>";
        }
        elseif ($item['type']=="drawer") {
            echo "<a class='site-links__item site-links__item--draw' data-flex='justify'><span>" . "{$item['title']}" . "</span><i class=plus></i></a>";
            echo "<div class='site-links__drop'>{$item['text']}</div>";
        }
        ?></li>
    <?php 
    endforeach;
    echo "</ul><!-- .site-links -->";
}

function _link( $l, $class = '' ) 
{
    $text = first_strip($l['native_name']);
    $href = $l['url'];
    return "<li class='{$class}'><a href='{$href}'><span>{$text}</span></a></li>";
}

// Get list of language(s)
function fuzion_langs() 
{
    $langs = icl_get_languages('skip_missing=1');

    if (1 < count($langs) ) 
    {
        foreach ($langs as $l) {
            if ( !$l['active'] ) {
                $list[] = _link($l);
            } else {
                $active = _link($l, '-current');
            }
        }
        echo '<ul class="fuzion-langs list list--block">'. $active .'<ul class="list list--block">'. join("\n", $list) .'</ul></ul>';
    }
}

function fuzion_product_related( $product ) 
{
    echo "<article class='single-product__cta-wrap' data-flex='row keep center'>";
    echo wp_get_attachment_image( $product['image']['id'], 'medium' );
    echo "<div class='single-product__cta'>";
        echo "<h2 class='section-title'>{$product['title']}</h2>";
        if ( $product['text'] ) echo "<p class='space-b'>{$product['text']}</p>";
        fuzion_cta_link( $product['cta_href'], $product['cta_text'], false );
    echo "</div></article>";
}

function fuzion_product_specs( $layout = 'simple_text', $product, $specs, $class = 'chapter__all' ) 
{
    if ( $layout === 'simple_text' ) {
        $text = $specs['text'];
        echo "<p class='{$class}'>{$text}</p>";
    } 
    elseif ( $layout === 'moldings' ) {
        $text = $specs['text'];
        echo "<div class='{$class}' data-flex=col>";
        echo "<p>{$text}</p>";
        echo "<ul class='molding-icons space-t list'>";
        foreach ($specs['items'] as $item) :
        $img = $item['image']['url'];
        echo "<li data-flex=col><img src='{$img}' alt>{$item['name']}</li>";
        endforeach;
        echo "</ul></div>";
    } 
    if ( strpos($layout, 'spec') !== false ) {
        echo "<dl class='single-product__spec {$class}'>";
        foreach ($product->get_attributes() as $tax => $attr) :
        $title = wc_attribute_label($tax);
        $name = $product->get_attribute( $attr->get_data()['name'] );
        echo "<dt>{$title}</dt><dd>{$name}</dd>";
        endforeach;
        foreach ($specs['items'] as $item) :
        echo "<dt>{$item['title']}</dt><dd>{$item['name']}</dd>";
        endforeach;
        echo "</dl>";
    }
    elseif ( strpos($layout, 'products') !== false ) {
        echo "<div class='{$class}'>";
        foreach ($specs['items'] as $product) :
        fuzion_product_related( $product );
        endforeach;
        echo "</div>";
    }
}

function fuzion_inner_search_form()
{
    ?>
    <form class="fuzion-filters__search" data-flex="row keep align" action>
        <input type="text" autocomplete>
        <button class="btn--clean btn--search">
        <?php include get_icons_directory('i-search.svg') ?>
        </button>
    </form>
    <?php
}

function fuzion_checkout_form_coupon() 
{
    ?>
    <div class="coupon">
        <?php /*<label for="coupon_code"><?php esc_html_e( 'Coupon:', 'woocommerce' ); ?></label> */ ?>
        <input type="text" name="coupon_code" class="input" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" id="coupon_code" value="" />
        <button type="submit" class="submit" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_html_e( 'Apply coupon', 'woocommerce' ); ?></button>
    </div>
    <?php
}

function fuzion_product_form_cart( $product ) 
{
    ?>
    <form class="product-form product-form--cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
        <?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

        <button class="btn--clean alt" data-flex="row keep center" type="submit" name="add-to-cart" value="<?= esc_attr( $product->get_id() ); ?>">
            <?php include get_icons_directory('i-cart.svg') ?>
            <span><?= esc_html( $product->single_add_to_cart_text() ) ?></span>
        </button>

        <?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
    </form>
    <?php
}

//
// Site's header
//

if ( ! function_exists( 'storefront_header_container' ) ) {
    function storefront_header_container() 
    {
        echo '<div class="header-full">';
    }
}

if ( ! function_exists( 'storefront_site_branding' ) ) {
	function storefront_site_branding() {
        $lang_code = ICL_LANGUAGE_CODE;
        if ($lang_code == 'en') $lang_code = '';
		?>
		<div class="fuzion-header__col fuzion-header__col--relative">
            <a class="header-logo" href="<?= site_url().'/'.$lang_code ?>">
            <?php include get_icons_directory('logo.svg') ?>
            </a>
		</div>
		<?php
	}
}

//
// Site's menus
//

$siteUrl = $_SERVER['HTTP_HOST'];
if (strpos($siteUrl, 'professional.') !== false) {
    $mainMenu = get_field('main_menu_professional', 'options');
    $secondaryMenu = get_field('secondary_menu_professional', 'options');
}
else {
    $mainMenu = get_field('main_menu_homeowner', 'options');
    $secondaryMenu = get_field('secondary_menu_homeowner', 'options');

}

// TODO: Make sure to use appropriate {main_menu}
$menus = array(
    'main' => $mainMenu,
    'pages' => $secondaryMenu,
    'helper' => get_field('footer_menu', 'options'),
    'social' => get_field('social_menu', 'options'),
);

// Creates action buttons
// @require get_icons_directory()
// @TODO SVG symbols  
function fuzion_header_icons() 
{
    ?>
    <ul class="header-icons list">
        <li><button aria-label=Search class="js-search-button btn--clean btn--icon" data-flex>
            <?php include get_icons_directory('i-search.svg') ?>
        </button></li>
        <li><a href="<?php echo site_url('/cart') ?>" aria-label=Cart class="btn--clean btn--icon" data-flex>
            <span id="cart-items"><?= WC()->cart->get_cart_contents_count(); ?></span>
            <?php include get_icons_directory('i-cart.svg') ?>
        </a></li><?php /*
        <li><button aria-label=Favourites class="btn--clean btn--icon">
            <?php include get_icons_directory('i-fav.svg') ?>
        </button></li> */ ?>
        <li><button aria-label=Menu class="btn--clean btn--icon btn--menu">
            <?php include get_icons_directory('i-menu.svg') ?>
        </button></li>
    </ul>
    <?php
}

// Creates social icons
// @require get_icons_directory()
// @TODO SVG symbols
function fuzion_social_icons( $share = false ) 
{
    global $menus;

    ?>
    <ul class="social-icons list">
        <?php foreach( $menus['social'] as $link ) : 
        ?>
        <li class="<?php echo $share ? $link['name'] . '-btn' : '' ?>"><a aria-label="<?= $link['name'] ?>" href="<?= $link['href'] ?>" target="_blank" rel="noopener">
            <?php include get_icons_directory("s-{$link['name']}.svg") ?>
        </a></li>
        <?php endforeach; 
        
        if( $share ): ?>
            <li><a class="email-btn" aria-label="email" href="#" target="_blank" rel="noopener">
                <?php include get_icons_directory("s-letter.svg") ?>
            </a></li>
        <?php
        endif;
        ?>
    </ul>
    <?php
}

function menu_permalink( $ID ) {
    // $ID = $post['ID'] ? $post['ID'] : $post->ID;
    if ( get_permalink($ID) )
        return get_permalink($ID);
        return '/nope';
}

function is_menu_retail( $name ) {
    if ( strpos($name, 'Retailer') !== false || strpos($name, 'Trouver un') !== false || strpos($name, 'Projects') !== false || strpos($name, 'Projets') !== false )
        return true;
        return false;
}

// Creates main navigation
// @TODO SVG symbols
function fuzion_menu_main( $class = 'fuzion-menu', $retail = false, $main = false ) 
{
    global $menus;

    ?>
    <button class="btn--cta btn--back -hidden"><?php 
        include get_icons_directory('b-next.svg') 
    ?></button>
    <ul class="<?= $class ?> list">
        <?php 
            foreach( $menus['main'] as $links ) : 
            if ( !$retail || !is_menu_retail($links['name']) ) : 
        ?>
        <li>
            <a class="fuzion-menu__trigger" href="<?= esc_url($links['href']) ?>" data-flex>
                <span><?= $links['name'] ?></span>
                <?php include get_icons_directory('i-down.svg') ?>
            </a>
            <?php if ( ! $main ) : ?>
            <ul class="fuzion-submenu">
                <?php if ( $links['submenu'] ) foreach( $links['submenu'] as $link ) : ?>
                <li><a href="<?= esc_url($link['link']) ?>"<?php if (strpos($link['link'], 'matterport') !== false) echo ' target=blank rel=noopener' ?>><?= $link['name'] ?></a></li>
                <?php endforeach ?>
            </ul>
            <?php endif ?>
        </li>
        <?php endif; endforeach ?>
    </ul><!-- .<?= $class ?> -->
    <?php if ( $main ) : ?>
    <div class="fuzion-dropdown">
        <?php 
            foreach( $menus['main'] as $x => $links ) : 
            if ( !$retail || !is_menu_retail($links['name']) ) : 
        ?>
        <div class="fuzion-dropdown__wrap" hidden>
        <div class="fuzion-dropdown__body" data-flex="row keep end">

            <div aria-live="polite" class="fuzion-dropdown__text">
                <?php if ( $links['submenu'] ) foreach( $links['submenu'] as $link ) : ?>
                <p class="js-dropdown-description" hidden></p>
                <?php endforeach ?>
            </div>
            
            <ul class="fuzion-submenu list list--block">
                <?php if ( $links['submenu'] ) foreach( $links['submenu'] as $link ) : ?>
                <li class="js-description"><a href="<?= esc_url($link['extra_link_url']) ?>" data-description="<?= $link['description'] ?>"><?= $link['name'] ?></a></li>
                <?php endforeach ?>
            </ul>

            <div aria-live="polite" class="fuzion-collection">
                <?php if ( $links['submenu'] ) foreach( $links['submenu'] as $link ) : ?>
                <a href="<?= esc_url($link['extra_link_url']) ?>" hidden<?php if (strpos($link['extra_link_url'], 'matterport') !== false) echo ' target=blank rel=noopener' ?>>
                    <span class="btn--cta" style="--color-bg: rgba(255,255,255, .75);--color-fg: rgba(255,255,255, 1);" data-flex="row keep center justify">
                        <span><?= $link['extra_link_text'] ?></span>
                        <?php include get_icons_directory('b-next.svg') ?>
                    </span>
                    <picture data-flex>
                        <img src="<?= $link['image']['url'] ?>" alt="<?= $link['extra_link_text'] ?>">
                    </picture>
                </a>
                <?php endforeach ?>
            </div>
        </div>
        </div>
        <?php endif; endforeach ?>
    </div><!-- .fuzion-dropdown -->
    <?php endif;
}

// Creates pages navigation
function fuzion_menu_pages() 
{
    global $menus;
    ?>
    <ul class="fuzion-menu fuzion-menu--pages list">
        <?php foreach( $menus['pages'] as $link ) : ?>
        
            <li><a href="<?= esc_url($link['href']) ?>"><?= $link['name'] ?></a></li>
            
        <?php endforeach ?>
    </ul>
    <?php
}

// Creates helper navigation
function fuzion_menu_help() 
{
    global $menus;
    ?>
    <ul class="fuzion-menu fuzion-menu--footer list">
        <?php foreach( $menus['helper'] as $link ) : ?>
        
            <li><a href="<?= esc_url($link['href']) ?>"><?= $link['name'] ?></a></li>
            
        <?php endforeach ?>
    </ul>
    <?php
}


/**
 * Like get_template_part() put lets you pass args to the template file
 * Args are available in the tempalte as $template_args array
 * @param string filepart
 * @param mixed wp_args style argument list
 */
function hm_get_template_part( $file, $template_args = array(), $cache_args = array() ) {
    $template_args = wp_parse_args( $template_args );
    $cache_args = wp_parse_args( $cache_args );
    if ( $cache_args ) {
        foreach ( $template_args as $key => $value ) {
            if ( is_scalar( $value ) || is_array( $value ) ) {
                $cache_args[$key] = $value;
            } else if ( is_object( $value ) && method_exists( $value, 'get_id' ) ) {
                $cache_args[$key] = call_user_method( 'get_id', $value );
            }
        }
        if ( ( $cache = wp_cache_get( $file, serialize( $cache_args ) ) ) !== false ) {
            if ( ! empty( $template_args['return'] ) )
                return $cache;
            echo $cache;
            return;
        }
    }
    $file_handle = $file;
    do_action( 'start_operation', 'hm_template_part::' . $file_handle );
    if ( file_exists( get_stylesheet_directory() . '/' . $file . '.php' ) )
        $file = get_stylesheet_directory() . '/' . $file . '.php';
    elseif ( file_exists( get_template_directory() . '/' . $file . '.php' ) )
        $file = get_template_directory() . '/' . $file . '.php';
    ob_start();
    $return = require( $file );
    $data = ob_get_clean();
    do_action( 'end_operation', 'hm_template_part::' . $file_handle );
    if ( $cache_args ) {
        wp_cache_set( $file, $data, serialize( $cache_args ), 3600 );
    }
    if ( ! empty( $template_args['return'] ) )
        if ( $return === false )
            return false;
        else
            return $data;
    echo $data;
}

// Check if current product category has any subcategories
function fuzion_has_subcategories() {
    $cat = get_queried_object();
    $catID = $cat->term_id;
    $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
    $hasChildren = get_term_children($catID, $term->taxonomy);

    if (empty($hasChildren)) {
        return false;
    } else {
        return true;
    }
}


// Create a shipping notice on cart page
function fuzion_cart_notice() {
    echo "<div class='space-b--lg'>";
    echo "<p class='text-brown'>" . __('*Each sample board will measure approximately 10" x (plank width).', 'woocommerce') . "</p>";
    echo "<p class='text-brown'>" . __('*Please allow 1 – 3 weeks for delivery. U.S. customers: Unfortunately we do not offer online sample ordering at this time, any orders placed will be cancelled. We appreciate your understanding.', 'woocommerce') . "</p>";
    echo "</div>";
}
 
add_action( 'woocommerce_before_cart_table', 'fuzion_cart_notice' );

add_action( 'wp_head', 'fuzion_social_tags' );

function fuzion_social_tags() {
    if (is_single()) {

        global $post;
        // get the featured image

        if(get_the_post_thumbnail($post->ID, 'thumbnail')) {
            $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),array(600,600));
        }

        $custom_fields = get_post_custom(get_the_ID());
        $post_content = $custom_fields['builder_blog_0_text'][0] ? $custom_fields['builder_blog_0_text'][0] : '';
        $post_excerpt = $custom_fields['builder_blog_2_text'][0] ? $custom_fields['builder_blog_2_text'][0] : '';
        $description = wdm_custom_excerpt( $post_excerpt, $post_content );
        $type = $post->post_type;

        if($type == 'post') {
            // if you want to share post content as an article
            $type = 'article';
        }

        // set the meta tags which will be read by Social Media sites
        if(!empty($image[0])) { ?>
            <meta property="og:image" content="<?php echo $image[0]; ?>" />
            <meta itemprop="image" content="<?php echo $image[0]; ?>">
        <?php
        } ?>

        <meta property='og:title' content="<?php the_title(); ?>" data-dynamic='true' />
        <meta property='og:url' content='<?php the_permalink(); ?>' />
        <meta property='og:description' content="<?php echo $description ?>"  data-dynamic='true' />
        <meta property='og:type' content='<?php echo $type; ?>'  data-dynamic='true' />
        <meta property='og:image' content='<?php echo $image[0]; ?>' />
        <meta property='og:site_name' content='Fuzion' data-dynamic='true' />
        <meta property='og:image:type' content='image/png' data-dynamic='true'>
        <meta property='og:image:width' content='600'  data-dynamic='true' />
        <meta property='og:image:height' content='600'  data-dynamic='true' />
        <meta property='og:locale' content='en_US' />

        <meta name="twitter:card" content="<?php the_title(); ?>">
        <meta name='twitter:site' content='Fuzion'>
        <meta name='twitter:creator' content='Fuzion'>
        <meta name='twitter:title' content="<?php the_title(); ?>">
        <meta name='twitter:description' content="<?php echo $description ?>">
        <meta name='twitter:image' content='<?php echo $image[0]; ?>'>
        <meta name='twitter:domain' content='<?php the_permalink(); ?>'>
        
        <?php
    }
} 

function wdm_custom_excerpt($content, $excerpt){
    
    if ($excerpt) {
        $text = $excerpt;
    } else {
        $text = $content;
    }

    $text = wp_trim_words( $text, 55);
    $text = strip_shortcodes( $text );
    $text = strip_tags($text);
    $text = str_replace("\'", "'", $text);
    
    return $text;
}

// Alter search posts per page
function myprefix_search_posts_per_page($query) {
    if ( $query->is_search ) {
        $query->set( 'posts_per_page', '-1' );
    }
    return $query;
}
add_filter( 'pre_get_posts','myprefix_search_posts_per_page' );