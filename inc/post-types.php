<?php 

add_theme_support('post-thumbnails');

$supports = ['thumbnail', 'page-attributes'];
$postTypes = ['collections', 'projects', 'lookbooks','resources'];

foreach ( $postTypes as $type ) :
    add_post_type_support( $type, $supports ); 
    add_action( 'init', "create_posttype_{$type}" );
endforeach;

function create_posttype_collections() {
 
    register_post_type( 'collections', // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Collections' ),
                'singular_name' => __( 'Collection' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'collections'),
            'show_in_rest' => true,
        )
    );
}

function create_posttype_projects() {
 
    register_post_type( 'projects', // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Projects' ),
                'singular_name' => __( 'Project' )
            ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'projects'),
            'show_in_rest' => true,
        )
    );
}

function create_posttype_lookbooks() {
 
    register_post_type( 'lookbooks', // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Lookbooks' ),
                'singular_name' => __( 'Lookbook' )
            ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'lookbooks'),
            'show_in_rest' => true,
            'exclude_from_search' => true
        )
    );
}

function create_posttype_resources() {
 
    register_post_type( 'resources',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Resources' ),
                'singular_name' => __( 'Resource' )
            ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'resources'),
            'show_in_rest' => true,
            'exclude_from_search' => true
        )
    );
}


// Hooking up our function to theme setup

//add_action('init', 'create_posttype_lookbooks' );
//add_action('init', 'create_posttype_resources' );
