<?php 

if ( function_exists('acf_add_options_page') ) 
{
    acf_add_options_page();
}

if ( function_exists('add_theme_support') )
{
    add_image_size('4k', 3680, '', true); // Large Thumbnail
    add_image_size('hd', 1920, '', true); // Large Thumbnail
    add_image_size('half', 960, '', true); // Large Thumbnail
    add_image_size('mobile', 576, '', true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');
}

// Handle child pages
function fuzion_child_pages( $template ) 
{
    global $post;

    if ( ! $post->post_parent ) 
        return $template;
    
    $parentPage = get_post(
        reset(array_reverse(get_post_ancestors($post->ID)))
    );

    $parentSlug = $parentPage->post_name;

    $childTpl = locate_template([
        "page-{$parentSlug}-{$post->post_name}.php",
        "page-{$parentSlug}-child.php",
    ]);

    if ( $childTpl ) 
        return $childTpl;
        return $template;
}

// Handle pre-code 
function do_pre( $var ) 
{
    echo '<pre>';
    print_r($var);
    echo '</pre>';
}

// Print svg directory
function get_icons_directory( $file ) 
{
    $dir = IS_DEV 
        ? '/src/svg/' 
        : '/src/svg/'
    ;
    return get_stylesheet_directory() . $dir . $file;
}

// Print landing page url
function get_landing_link( $owner = true, $path = '' ) 
{
    return false === strpos(strtolower($owner), 'pro')
        ? get_field('homeowner_homepage_link', 'option')
        : get_field('professional_homepage_link', 'option')
    ;
}

// Show current template
function show_template() 
{
    global $template; 
    if ( IS_DEV && !is_admin() ) 
    echo "<p style='color: #000;padding: 10px;border-bottom: 1px solid;'>{
        $template
    }</p>";
}

// Hide admin top-bar
function remove_admin_bar() 
{
    show_admin_bar( is_admin() );
}


/*------------------------------------*\
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
\*------------------------------------*/

// Remove 'text/css' from our enqueued stylesheet
function remove_style_type($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Remove unnecessary syntax
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

add_filter('style_loader_tag', 'remove_style_type'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

add_filter('page_template', 'fuzion_child_pages');
add_action('after_setup_theme', 'remove_admin_bar');


// geo redirect
add_action('init', 'redirect_to_lang');
function redirect_to_lang() {

    try {
      
        if (!isset($_COOKIE['georedirect'])) {
       
            $ipaddress = getenv("REMOTE_ADDR"); 
            $ipInfo = unserialize(file_get_contents('http://ip-api.com/php/'.$ipaddress));

            // redirect USA to us store
            $lang = 'en-us';
            if ($ipInfo['countryCode']=="US" && ICL_LANGUAGE_CODE!=$lang) {
                setcookie("georedirect","yes",time() + 36000,'/');
                global $wp;
                $current_slug = add_query_arg( array(), $wp->request );
                header('Location: '.site_url().'/'.$lang.'/'.$current_slug);
                echo "<script>window.location.href='".site_url().'/'.$lang.'/'.$current_slug."';</script>";
                exit;
            }

            // redirect Quebec to fr store
            $lang = 'fr';
            if ((strtolower($ipInfo['regionName'])=="quebec" || strtolower($ipInfo['city'])=="quebec") && ICL_LANGUAGE_CODE!=$lang) {
                setcookie("georedirect","yes",time() + 36000,'/');
                global $wp;
                $current_slug = add_query_arg( array(), $wp->request );
                header('Location: '.site_url().'/'.$lang.'/'.$current_slug);
                echo "<script>window.location.href='".site_url().'/'.$lang.'/'.$current_slug."';</script>";
                exit;
            }

            // test a redirect from poland (TO DELETE AFTER)
            $lang = 'en-us';
            if ($ipInfo['countryCode']=="PL" && ICL_LANGUAGE_CODE!=$lang) {
                setcookie("georedirect","yes",time() + 36000,'/');
                global $wp;
                $current_slug = add_query_arg( array(), $wp->request );
                header('Location: '.site_url().'/'.$lang.'/'.$current_slug);
                echo "<script>window.location.href='".site_url().'/'.$lang.'/'.$current_slug."';</script>";
                exit;
            }

        }
    }
    catch (Exception $e) {

    }
}