<?php 

// Extend default loop
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

// Change `Cart` button text
add_filter( 'woocommerce_product_add_to_cart_text', 'change_add_to_cart_name' );

// Turn off product pagination
add_filter( 'theme_mod_storefront_product_pagination', '__return_false');

// Turn off sticky add to cart
add_filter( 'theme_mod_storefront_sticky_add_to_cart', '__return_false',9999);

// product category - change order of sections
add_action( 'woocommerce_before_shop_loop', 'result_count_remove_hook', 1 );
add_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 1 );

// Override product filters
add_action( 'woof_html_types_view_checkbox', 'override_woof_checkbox', 10 );

// Handle custom UI
add_filter('woocommerce_checkout_fields' , 'custom_checkout_fields');

function custom_checkout_fields( $fields ) {
  foreach ($fields as &$fieldset) {
    foreach ($fieldset as &$field) {
      $field['class'][] = 'input-group';
      if ( $field['type'] !== 'textarea' )
        $field['input_class'][] = 'input--plain';
      else 
        $field['input_class'][] = 'input--textarea';
    }
  }
  return $fields;
}

// WOOF Filter checkbox - change: don't show the options for filters with 0 quantity
function override_woof_checkbox() {
	return get_stylesheet_directory().'/woocommerce-products-filter/html_types/checkbox.php';
}
 
function change_add_to_cart_name() {
	return "Order Sample";
}

function result_count_remove_hook(){
  remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
}

function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options –> Reading
  // Return the number of products you wanna show per page.
  $cols = 9999;
  return $cols;
}


add_filter( 'wpsl_templates', 'custom_templates' );

function custom_templates( $templates ) {

    /**
     * The 'id' is for internal use and must be unique ( since 2.0 ).
     * The 'name' is used in the template dropdown on the settings page.
     * The 'path' points to the location of the custom template,
     * in this case the folder of your active theme.
     */
    $templates[] = array (
        'id'   => 'custom',
        'name' => 'Custom template',
        'path' => get_stylesheet_directory() . '/' . 'wpsl-templates/custom.php',
    );

    return $templates;
}

// Remove tax from cart page
add_action( 'wp', function() {

    if ( class_exists( 'woocommerce') ) {
        if ( is_cart() ) {
            add_filter( 'wc_tax_enabled', '__return_false' );
        }
    }

});


// Removes ability to reorder support items only for product categories
add_action('admin_head', 'admin_css');
function admin_css() {
  echo '<style>
    .wp-admin.post-type-product.taxonomy-product_cat .acf-row-handle.order {
      display: none;
    }
  </style>';
}

// Checking and validating when updating cart item quantities when products are added to cart
add_filter( 'woocommerce_update_cart_validation', 'only_five_items_allowed_cart_update', 10, 4 );
function only_five_items_allowed_cart_update( $passed, $cart_item_key, $values, $updated_quantity ) {

    $cart_items_count = WC()->cart->get_cart_contents_count();
    $original_quantity = $values['quantity'];
    $total_count = $cart_items_count - $original_quantity + $updated_quantity;

    if( $total_count > 5 ){
        // Set to false
        $passed = false;
        // Display a message
        add_action("woocommerce_cart_contents", "fuzion_limit_message", 1000);

        function fuzion_limit_message( ) {
          echo '<p class="limit-message">A maximum of 5 samples can be ordered at one time.</p>'; 
        }
    }
    return $passed;
}

// -------------------------- Start
// Custom Checkout Field
// ------------------------

// Add select field to the checkout page
add_action('woocommerce_after_checkout_billing_form', 'wps_add_select_checkout_field');
function wps_add_select_checkout_field( $checkout ) {

  echo '<h3 class="section-title hgroup hgroup--space" style="margin-top: 20px;">Customer Type</h3>';

  echo '<div class="woocommerce-inner__form--billing">';

	woocommerce_form_field( 'customerType', array(
      'type'          => 'select',
      'required'    => true,
	    'class'         => array( 'wps-drop', 'form-row-wide' ),
	    'label'         => __( 'Select one option' ),
	    'options'       => array(
	    	'blank'		=> __( 'Select one', 'fuzion' ),
          'homeowner'	=> __( 'Homeowner', 'fuzion' ),
	        'builder'	=> __( 'Builder', 'fuzion' ),
	        'designer' 	=> __( 'Designer', 'fuzion' ),
	        'architect' 	=> __( 'Architect', 'fuzion' )
	    )
    ),

	$checkout->get_value( 'customerType' ));

  echo '</div>';

}

// Process the checkout
add_action('woocommerce_checkout_process', 'wps_select_checkout_field_process');
function wps_select_checkout_field_process() {
   global $woocommerce;

    // Check if set, if its not set add an error.
    if ($_POST['customerType'] == "blank")
    wc_add_notice( '<strong>Please select option</strong>', 'error' );

}

// Update the order meta with field value
add_action('woocommerce_checkout_update_order_meta', 'wps_select_checkout_field_update_order_meta');
function wps_select_checkout_field_update_order_meta( $order_id ) {

  if ($_POST['customerType']) update_post_meta( $order_id, 'customerType', esc_attr($_POST['customerType']));

}

// Display field value on the order edition page
add_action( 'woocommerce_admin_order_data_after_billing_address', 'wps_select_checkout_field_display_admin_order_meta', 10, 1 );
function wps_select_checkout_field_display_admin_order_meta($order){

	echo '<p><strong>'.__('Delivery option').':</strong> ' . get_post_meta( $order->id, 'customerType', true ) . '</p>';

}

// Add selection field value to emails
add_filter('woocommerce_email_order_meta_keys', 'wps_select_order_meta_keys');
function wps_select_order_meta_keys( $keys ) {

	$keys['customerType:'] = 'customerType';
  return $keys;
  
}

// -------------------------- End