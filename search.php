<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package storefront
 */

get_header();

?><div class="search-page" data-grid="top"> 

    <div class="search-page__title">
        <h1 class="fuzion-title">‘<?php the_search_query(); ?>’</h1>
    </div>

    <div class="search-list">

<?php

    // Product SKU
    $product_id = wc_get_product_id_by_sku($s);
    $searchItems = array();

    if($product_id):
        
        $product = wc_get_product( $product_id );
        $productName = $product->get_name();
        $link = get_permalink( $product->get_id() );
        $image = $product->get_image_id();

        array_push($searchItems, array(
			'title' => $productName,
			'cta_href' => $link,
            'img_id' => $image,
            'post_type' => 'product'
          ));
      
        ?>

        <?php
    endif;


    // Product Category
    $terms = get_terms( 'product_cat', array(
        'name__like' => $s,
        'hide_empty' => false
    ));

    if ( count($terms) > 0 ) :
        foreach ( $terms as $term ):

        $custom_fields = get_field( 'builder_banners', $term);

        array_push($searchItems, array(
            'title' => get_term( $term )->name,
            'cta_href' =>  get_term_link( $term ),
            'img_id' =>$custom_fields[0]['image']['ID'],
            'post_type' => 'product category'
            ));
        ?>

    <?php   
        endforeach;
    endif;


    // Producst, Pages, CPT
    while ( have_posts() ) :
        the_post();

        global $post;

        $post_type = get_post_type();
        $already_has_thumb = has_post_thumbnail(get_the_id());

        $custom_fields = get_post_custom($post->id);
        $image_carousel = $custom_fields['builder_banners_0_carousel_0_background_image'][0] ? $custom_fields['builder_banners_0_carousel_0_background_image'][0] : '';
        $image = get_post_thumbnail_id(get_the_id()) ? get_post_thumbnail_id(get_the_id()) : $image_carousel;

        array_push($searchItems, array(
            'title' => get_the_title(),
            'cta_href' =>  get_the_permalink(),
            'img_id' => $image,
            'post_type' => $post_type
        ));

    endwhile; 

    asort($searchItems);

    set_query_var('search_items', $searchItems);
    get_template_part('parts/search', 'results');

?>    </div><!-- search-results ends -->
</div><!-- search-page ends --> <?php

do_action( 'storefront_sidebar' );
get_footer();
