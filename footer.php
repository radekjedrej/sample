    </div><!-- #content -->

    <?php do_action( 'storefront_before_footer' ); ?>

	<footer class="fuzion-footer" role="contentinfo" data-flex="col">
		<div class="fuzion-footer__row fuzion-footer__row--info" data-flex="row">

            <div class="fuzion-footer__col fuzion-footer__col--info">
                <form class="fuzion-footer__form" action="https://fuzionflooring.us12.list-manage.com/subscribe/post?u=3fb035c9da8499f999478b5ca&amp;id=26fab609ab" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div class="form">
                        <label class="legend" for="mce-EMAIL"><?= __('Sign up to Newsletter', 'footer') ?></label>
                        <input required type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="<?= __('Email Address', 'footer') ?>">
                        <button name="subscribe" id="mc-embedded-subscribe" type=submit><?= __('Send', 'footer') ?></button>
                    </div>

                    <div class="fuzion-footer__info" id="mce-responses">
                        <div class="fuzion-title response" id="mce-error-response" style="display:none"></div>
                        <div class="fuzion-title response" id="mce-success-response" style="display:none"></div>
                    </div>

                    <div style="position: absolute; left: -5000px;" aria-hidden="true">
                        <input type="text" name="b_3fb035c9da8499f999478b5ca_26fab609ab" tabindex="-1" value="">
                    </div>

                </form><!-- .fuzion-footer__form -->
                <nav class="fuzion-footer__nav" data-flex="row center space">
                    <?php 
                        fuzion_menu_pages();
                        fuzion_social_icons();
                    ?>
                </nav><!-- .fuzion-footer__nav -->
            </div>
            
            <nav class="fuzion-footer__col fuzion-footer__col--links" data-flex="row">
                <a class="fuzion-footer__cta" href="<?= get_landing_link('pro') ?>">
                    <span><?= 
                        __('Fuzion for <br>Professionals', 'footer') 
                    ?></span>
                </a>
                <a class="fuzion-footer__cta" href="<?= get_landing_link() ?>">
                    <span><?= 
                        __('Fuzion for <br>Homeowners', 'footer') 
                    ?></span>
                </a>
            </nav><!-- .fuzion-footer__col -->

        </div><!-- .fuzion-footer__col -->

        <div class="fuzion-footer__row fuzion-footer__row--copy" data-flex="row space">

            <p class="fuzion-footer__col fuzion-footer__col--info">
                <?= get_field('copyright_text', 'options') ?>
            </p><!-- .fuzion-footer__col -->
            
            <nav class="fuzion-footer__col fuzion-footer__col--nav">
                <?php 
                    fuzion_menu_help(); 
                ?>
            </nav><!-- .fuzion-footer__nav -->

        </div><!-- .fuzion-footer__col -->
	</footer><!-- .footer -->

	<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
