<?php
/**
 * The template for displaying all single posts.
 *
 * @package storefront
 */

$dark_header = true;

get_header(); ?>

	<main data-grid=head><?php

        fuzion_layout('builder_banners');
        fuzion_layout('builder_body');
        fuzion_layout('builder_support');

    ?>
	</main>

<?php
do_action( 'storefront_sidebar' );
get_footer();