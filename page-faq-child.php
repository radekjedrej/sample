<?php
/**
 * The template for FAQ.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package storefront
 */

get_header();

    while ( have_posts() ) :
        the_post();

        echo '<main data-grid=top>';

            $cta_href = get_field('cta_href') ? get_field('cta_href') : '';

            set_query_var('banner_copy', array(
                'title' => get_the_title(),
                'cta_href' => $cta_href,
                'cta_text' => __('View all Products', 'fuzion')
            ));
            get_template_part('parts/banner', 'inner');

            if (have_rows("questions")) :
                while (have_rows("questions")) : the_row();  
                
                set_query_var('accordion', array(
                    'title' => get_sub_field("question"),
                    'text' => get_sub_field("answer")
                ));
                get_template_part('parts/text'/*, 'accordion'*/);
                
                endwhile;
            endif; 

        echo '</main>';

    endwhile; 
    
do_action( 'storefront_sidebar' );
get_footer();
