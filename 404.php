<?php
/**
 * The template for displaying 404.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package storefront
 */

get_header();

    // while ( have_posts() ) : the_post();

        do_action( 'storefront_page_before' );

        $about = get_page_by_title('About');
        $image = get_field('builder_body', $about->ID)[0]['image'];

        $sliderItems = array(
            array(
                'index' => '',
                'title' => __('<em>Oops!</em> 404', 'fuzion'),
                'text' => __('The page you were looking for appears to have been moved, deleted or does not exist. You could go back to where you were or head over to our homepage for more information.', 'fuzion'),
                'background_image' => $image
            )
        );

        set_query_var('slider_items', $sliderItems);
        get_template_part('parts/banner');

        /**
         * Functions hooked in to storefront_page_after action
         *
         * @hooked storefront_display_comments - 10
         */
        do_action( 'storefront_page_after' );

    // endwhile; 
    
do_action( 'storefront_sidebar' );
get_footer();
