<!doctype html>
<html <?php language_attributes() ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ) ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '2354633228121712');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=2354633228121712&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109665902-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-109665902-1');
</script>

<?php wp_head() ?>
</head>

<body <?php body_class() ?>>

<?php 
	wp_body_open();

	do_action( 'storefront_before_site' );

		get_template_part('parts/header', 'nav');

	if ( is_woocommerce() )
		get_template_part('parts/header', 'nav-products');
		get_template_part('parts/header', 'nav-empty');
	
?>

<div id="page" class="hfeed site">
	<?php 
	do_action( 'storefront_before_header' );
	?>
	<header class="<?= fuzion_header_class($post) ?>">

		<div class="fuzion-header__col fuzion-header__col--relative header-nav">
			<?php if ( !is_front_page() )
				fuzion_menu_main('fuzion-menu fuzion-menu--main', true, true) 
			?>
		</div>

		<?php storefront_site_branding('fuzion-header__col') ?>

		<div class="fuzion-header__col fuzion-header__col--relative" data-flex="col">
			<?php 
				fuzion_langs();
				
				if ( !is_front_page() ) 
				fuzion_header_icons();
			?>
		</div>

	</header><!-- .fuzion-header -->

	<div class="fuzion-search" aria-hidden="true" data-flex="col">
		<?php get_search_form(); ?>
	</div><!-- .fuzion-search -->

	<?php
	/**
	 * Functions hooked in to storefront_before_content
	 *
	 * @hooked storefront_header_widget_region - 10
	 * @hooked woocommerce_breadcrumb - 10
	 */
	do_action( 'storefront_before_content' );
	?>

	<div id="content" class="site-content">
		
		<?php
		do_action( 'storefront_content_top' );
