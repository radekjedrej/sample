<?php 

define('IS_DEV', strpos($_SERVER['HTTP_HOST'], 'local') !== false);



// Basic functions
require 'inc/base.php';

// Handle custom assets
require 'inc/assets.php';

// Handle custom post types
require 'inc/post-types.php';

// AJAX call
require 'inc/ajax.php';

// Template functions
require 'inc/theme.php';

// Template overrides
require 'inc/overrides.php';

require 'inc/menus.php';



?>