<?php
/**
 * The template used for displaying all projects in template-projects.php
 *
 * @package storefront-child
 */

$custom_fields = get_post_custom($post->id);
        
$project_name = $custom_fields['builder_banners_0_name'][0] ? $custom_fields['builder_banners_0_name'][0] : '';
$client_name = $custom_fields['builder_banners_0_client'][0] ? $custom_fields['builder_banners_0_client'][0] : '';
$product_name = $custom_fields['builder_banners_0_product'][0] ? $custom_fields['builder_banners_0_product'][0] : '';
$image = $custom_fields['builder_banners_0_image'][0] ? $custom_fields['builder_banners_0_image'][0] : '';
$image = wp_get_attachment_image($image, "half", "", array( "class" => "projects__img" ));
$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);

?>

<a href="<?php the_permalink(); ?>" class="projects__box projects__box--<?= $template_args['count']; ?> hover-link">

    <div class="project__content">

        <div class="projects__media">
        <?= $image ?>
        </div>

        <div class="projects__details">

            <div class="projects__info" data-flex="row keep center justify">

                <div class="projects__title">
                <h3 class="section-title hover-link--color"><?= $project_name; ?></h3>
                </div>

                <div class="projects__collection">
                <p><?= $product_name; ?></p>
                </div>
            </div>

            <div class="projects__client">
                <p><?= $client_name; ?></p>
            </div>

        </div>

    </div>
</a> 