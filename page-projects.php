<?php

get_header();

    $perPage = 6;

    while ( have_posts() ) :
        the_post();

        fuzion_layout('builder_banners');

        $projectEvents = new WP_Query( array(  
            'paged' => get_query_var('paged', 1),
            'post_type' => 'projects',
            'post_status' => 'publish',
            'posts_per_page' => $perPage, 
            'orderby' => array(
            'date' =>'DESC',
            )
        )); 

        ?>

        <div class="projects-list space-t--xl" data-grid> 

            <div class="projects"
            data-action="load_more_posts"
            data-page="<?= get_query_var('paged') ? get_query_var('paged') : 1; ?>"
            data-max="<?= $projectEvents->max_num_pages; ?>"
            > 

            <?php
            $count = 1;
            while ( $projectEvents->have_posts() ) : $projectEvents->the_post(); 
            ?>

                <?php hm_get_template_part( 'content-projects', [ 'count' => $count ] ); ?>
                
            <?php
            $count++;
            $count = $count === 5 ? $count = 1 : $count;
            endwhile;
            wp_reset_postdata(); 
            echo '</div><!-- .projects -->';

            if ( $projectEvents->found_posts > $perPage ) : ?>
            <div class="projects__btn">
                <?php fuzion_cta_link( '', '', false, 'js-load-more btn--down' ); ?>
            </div>
            <?php 
            endif;

        echo '</div><!-- .projects-list -->';

    endwhile; 
    
do_action( 'storefront_sidebar' );
get_footer();
