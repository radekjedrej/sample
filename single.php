<?php
/**
 * The template for displaying all single posts.
 *
 * @package storefront
 */


$custom_fields = get_post_custom(get_the_ID());
$nextPost = get_permalink(get_adjacent_post(false,'',true));


get_header(); ?>

    <main>

        <div class="single-banner">
            <div class="single-banner__bg"></div>
            <div data-grid=wide>
                <div class="single-banner__content">
                    <a class="text-white" href="<?= get_site_url(); ?>/journal"><?= __('Back to Journal', 'fuzion') ?></a>
                    <h1 class="js-banner-title fuzion-title"><?= the_title(); ?></h1>
                    <picture>
                        <?php
                            $image = $custom_fields['builder_blog_1_image'][0] ? $custom_fields['builder_blog_1_image'][0] : '';
                            $sliderImage = get_post_thumbnail_id(get_the_ID()) ? get_post_thumbnail_id(get_the_ID()) : $image;
                        ?>
                        <?= wp_get_attachment_image( $sliderImage, 'hd' ) ?>
                    </picture>
                </div>
            </div>
        </div>

        <?php 
            fuzion_layout('builder_blog', 'parts/blog'); 
        ?>

        <div class="single-navigation__wrapper">

            <div class="single-navigation" data-grid=wide data-flex>

                <!-- Social share icons -->
                <?php get_template_part('parts/share', 'icons'); ?>

                <!-- Next post btn -->
                <a class="next-post" href="<?= $nextPost ?>">
                    <span class="section-title"><?= __('Next Blog', 'fuzion') ?></span>
                </a>
            </div>
            <div class="single-navigation--bg"></div>
        </div>

	</main>

<?php
do_action( 'storefront_sidebar' );
get_footer();