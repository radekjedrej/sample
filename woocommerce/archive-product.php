<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

$cat = get_queried_object();
$catID = $cat->term_id;

// case when filters are applied, $cat returns a different object
if ($catID === null) {
	$url = explode('/',$_SERVER['REQUEST_URI']);
	$cat = get_term_by( 'slug', $url[2], 'product_cat' );
}

$parentCat = get_term($cat->parent, $cat->taxonomy);

// Support menu
$supportItems = get_field('support_items', 'term_'.$catID);

if (fuzion_has_subcategories()):

	if ( $post = get_page_by_path( $cat->slug, OBJECT, 'collections' ) ) :
    	$collectionID = $post->ID;
	else:
		$collectionID = 0;
	endif;
	
	$supportItems = get_field('support_items', $collectionID);

endif;

// Prepare proper slider objects
$sliderItems = array(); 

if( have_rows('builder_banners',$cat->taxonomy."_".$cat->term_id) ):
    while ( have_rows('builder_banners',$cat->taxonomy."_".$cat->term_id) ) : the_row();

        array_push($sliderItems, array(
			'title' => get_sub_field('title'),
			'index' => get_sub_field('subtitle'),
			'text' => get_sub_field('text'),
			'background_image' =>  wp_is_mobile() ? get_sub_field('image_mobile') : get_sub_field('image') ,
		));

    endwhile;
endif;

set_query_var('slider_items', $sliderItems);
set_query_var('slider_aside', $supportItems);
get_template_part('parts/banner');

// If not found
$not_found_txt = __('Ooops! No products found.', 'woocommerce');
echo "<h5 id=noproducts data-grid class='fuzion-title space-t--xl space-b--xl d-none'>{$not_found_txt}</h5>";

// Product Filters
$hideFilters = "";
if ($cat->parent>0) $hideFilters = ' hide-filters';

echo '<div class="fuzion-products space-t--lg space-b--lg" data-grid>';

if ( woocommerce_product_loop() ) { ?> 

	<div class="fuzion-nav__trigger-wrap" data-flex="row keep center justify">
		<a class="product-filters__trigger tabs-product__control"><?php include get_icons_directory('i-filter.svg') ?><span class="tabs-filter__title section-title">Filter</span></a>
	<?php

	$collectionsSlug = $cat->slug;
	$collection = get_page_by_path( $collectionsSlug, OBJECT, 'collections' );

	if ( fuzion_has_subcategories() ) fuzion_cta_link(
		get_permalink($collection->ID),
		__('View our Collections', 'woocommerce'),
		false, 'btn--white-space'
	);

	echo '</div><!-- .fuzion-nav__trigger-wrap -->' .
		 '<div class="fuzion-filters fuzion-filters--dropdowns'.$hideFilters.'" data-flex="row">';

	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked woocommerce_output_all_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	do_action( 'woocommerce_before_shop_loop' );

	echo '<div id=filters></div>';

	woocommerce_product_loop_start();

	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 */
			do_action( 'woocommerce_shop_loop' );

			wc_get_template_part( 'content', 'product' );
		}
	}

	woocommerce_product_loop_end();

	echo '</div><!-- .fuzion-filters -->';

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	
	// TO REMOVE RESULT COUNT IN THE BOTTOM
	// do_action( 'woocommerce_after_shop_loop' );
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	// do_action( 'woocommerce_no_products_found' );
	echo "<script>setTimeout(() => { window.ProductsNotFound && window.ProductsNotFound() }, 0)</script>";
}

echo '</div><!-- .fuzion-products -->';

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
do_action( 'woocommerce_sidebar' );

get_footer( 'shop' );