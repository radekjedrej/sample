<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

$cat = get_queried_object();
$catID = $cat->term_id;

$cats = get_the_terms($post->ID, 'product_cat');

$parentCat = get_term($cat->parent, $cat->taxonomy);

$supportItems = get_field('support_items', $product->get_id());
$productSpecs = get_field('data_fields', $product->get_id());


/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<section id="product-<?php the_ID(); ?>" <?php wc_product_class( 'single-product', $product ); ?> data-grid="top">

	<p class="single-product__info">
		<?php /* 
		This is returning the opposite order that we want. I can't find a way to fix it. Let me know if you can change this with front-ent
		*/ 
		echo wc_get_product_category_list( $product->get_id(), ' | ', '<span class="single-product__cat">', '</span>' ); ?>
	</p>

	<header class="product-head product-head--single">
		<h2 class="section-title"><?= $product->get_name(); ?></h2>
		<span class="product-head__sku"><?= 
			( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'woocommerce' ) 
		?></span>
	</header>

	<section class="single-product__banner" data-flex="row justify">
		<?php
			$imageId = get_post_meta( $product->get_id(), 'room_scene', true );/* 
		
		"half" might look that it's getting the full size image, but I think it's because the uploaded image was not big enough, so there's no actual version of "half" for this image, but you can try with mobile and it works -->
		*/ ?>

		<div class="single-product__main">
			<div class="slide-wrap" data-slider="product">
				<div class="product__img--main">
					<img class="js-product__img product__img--shadow" src="<?= wp_get_attachment_image_src( $product->get_image_id(),'half')[0] ?>" />
				</div>
				
				<div class="product__img--secondary">
					<img src="<?= wp_get_attachment_image_url($imageId, 'half' ) ?>" />
				</div>
			</div>

			<div class="single-product__thumbnail">
				<img class="js-product__side-img" src="<?= wp_get_attachment_image_url($imageId, 'half' ) ?>" alt="">
				<span class="icon-glass">
					<?php include get_icons_directory('i-search.svg') ?>
				</span>
			</div>
		</div>

		<div class="single-product__aside" data-flex=col>

			<?php fuzion_site_links( $supportItems, __('Support Items', 'fuzion') ) ?>
			
		</div>
	</section><!-- .single-product__banner -->

	<div class="product-links product-links--space space-b" data-flex="row justify">
		<div class="product-links__cart" data-flex="row center">
			
			<?php 
			global $woocommerce;
			$hiddenDiv = '<div style="display:none;">';
			$hiddenCloseDiv = "</div>";
			$disablePurchase = get_field('disable_purchase', 'option');
			
			if ( $product->is_in_stock() && $disablePurchase==false /*&& $woocommerce->cart->cart_contents_count < 5 */ ) {
				$hiddenDiv = '';
				$hiddenCloseDiv = '';
			} ?>
				<?php 

				do_action( 'woocommerce_before_add_to_cart_form' );
				echo $hiddenDiv;
				fuzion_product_form_cart( $product );
				echo $hiddenCloseDiv;
				do_action( 'woocommerce_after_add_to_cart_form' );

				// necessary, otherwise the cart link on the top doesn't work
				echo '<div style="display:none;>'; 
				do_action( 'woocommerce_after_shop_loop_item' );
				echo '</div>';
				?>
		
		</div>
		<div class="product-links__share" data-flex="row keep center">
			<p><?= __('Share ', 'fuzion') ?></p>
			<?php fuzion_social_icons() ?>
		</div>

	</div><!-- .product-links -->
	<?php 

	// Prepare specifications
	foreach ($productSpecs as $specs) :
	$layout = $specs['acf_fc_layout'];
	$title = $specs['title'] ? $specs['title'] : __('Cleaning products', 'fuzion-product'); 
	$class = 'chapter__all';
	?>
	<article class="chapter chapter--cols" data-flex="row justify">
		<h1 class="chapter__heading section-title"><?= $title ?></h1><?php 

		fuzion_product_specs( $layout, $product, $specs ) ?>
		
	</article>
	<?php endforeach ?>

</section>

<div class="related-products slider"><?php 
	foreach ($cats as $term) {
		$productCatId = $term->term_id;
		$productCatSlug = $term->slug;
		$productCatName = $term->name;
		break;
	}

	$products = wc_get_products(array(
		'category' => array($productCatSlug),
	));

	// Prepare proper slider objects
	$sliderTitle = __('View more swatches', 'fuzion');
	$sliderItems = array(); foreach ($products as $_product) :
		if ($_product->get_id()==$product->get_id()) continue;
		array_push($sliderItems, array(
			'title' => $_product->get_name(),
			'image' => ['id' => $_product->get_image_id()],
			'cta_text' => $productCatName .' '. __('Collection', 'fuzion'),
			'cta_href' => get_permalink( $_product->get_id() )
		));
	endforeach;

	set_query_var('slider_title', $sliderTitle);
	set_query_var('slider_items', $sliderItems);
	get_template_part('parts/image-carousel', 'nav');

?></div>