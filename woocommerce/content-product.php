<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<li <?php wc_product_class( '', $product ); ?> data-flex=col>

	<?php 
	/**
	 * Hook: woocommerce_before_shop_loop_item_title.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	?>
	<a class="hover-link" href="<?= get_permalink( $product->get_id() ); ?>">
		<div class="product-thumb">
			<picture class="product__image">
				<?php do_action( 'woocommerce_before_shop_loop_item_title' ); ?>
			</picture>
			<div class="product__cover"></div>
		</div>
		<header class="product-head" data-flex="row center keep">
			<h2 class="section-title hover-link--color"><?= $product->get_title() ?></h2>
			<span class="product-head__sku"><?= $product->get_sku(); ?></span>
		</header>
	</a>

	<ul class="product-attributes list list--block"><?php 

		$cats = get_the_terms( $product->get_id(), 'product_cat' );
		foreach ($cats as $cat) : 
			if ($cat->parent!=0) : ?>

			<li><?= __('Collection: ', 'fuzion') . $cat->name ?></li>

		<?php endif; 
		endforeach;

		foreach ($product->get_attributes() as $taxonomy => $attr) :
			if ($attr->get_data()['visible']) :
				$label = wc_attribute_label($taxonomy);
				$info = $product->get_attribute( $attr->get_data()['name'] );
			?>

			<li><?= $label ?>: <?= $info ?></li>

			<?php endif;
		endforeach;

	?></ul><!-- .product-info --><?php 
	
	/**
	 * Hook: woocommerce_after_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	?>

	<div class="product-links" data-flex>
		<span class="product-btn product-btn--view">
			<a href="<?= get_permalink( $product->get_id() ); ?>"><?= 
				__('View Product', 'fuzion') 
			?></a>
		</span>
		<span class="product-links__cart" data-flex="row keep center">
			<?php 
			global $woocommerce;

			$availability = $product->get_availability();
			$hiddenDiv = '<div style="display:none;">';
			$hiddenCloseDiv = "</div>";
			$disablePurchase = get_field('disable_purchase', 'option');
			if($availability['availability'] != 'Out of stock' && $disablePurchase==false /*&& $woocommerce->cart->cart_contents_count < 5*/ ) {
				$hiddenDiv = '';
				$hiddenCloseDiv = '';
			} 
				echo $hiddenDiv;
				include get_icons_directory('i-cart.svg');
				
				do_action( 'woocommerce_after_shop_loop_item' );
				echo $hiddenCloseDiv;
				
			?>
		</span>
	</div>
</li>
