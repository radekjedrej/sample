const 
    path = require('path'),
    autoprefixer = require('autoprefixer'),
    MiniCssExtractPlugin = require('mini-css-extract-plugin'),
    UglifyJSPlugin = require('uglifyjs-webpack-plugin'),
    OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin'),
    BrowserSyncPlugin = require('browser-sync-webpack-plugin')
;

const config = Object.freeze({
    proxy   : 8888,
    port    : 3000,
    src     : './src/js/',
    dev     : process.env.NODE_ENV || 'development',
})

const sassOptions = {}

if ( ! config.dev.includes('dev') ) 
    sassOptions.outputStyle = 'compressed'

module.exports = {

    entry: {
        front: './src/index.js',
        collection: `${config.src}page/collection.js`,
        resources: `${config.src}page/resources.js`,
        product: `${config.src}page/product.js`,
        socialShare: `${config.src}page/socialShare.js`,
        blog: `${config.src}page/blog.js`,
        careMaintenance: `${config.src}page/careMaintenance.js`,
    },

    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].min.js'
    },

    resolve: {
        extensions: ['.js', '.scss']
    },

    module: {
        rules: [
            {
              test: /\.js$/,
              exclude: /(node_modules)/,
              use: {
                loader: 'babel-loader',
                options: {
                  presets: ['@babel/preset-env']
                }
              }
            },
            {
              test: /\.(sa|sc|c)ss$/,
              use: [
                process.env.NODE_ENV === 'development'
                ? 'style-loader'
                : MiniCssExtractPlugin.loader,
                {
                    loader: 'css-loader',
                    options: {
                      importLoaders: 2,
                    },
                },
                {
                    loader: 'postcss-loader'
                },
                {
                    loader: 'sass-loader',
                    options: { sassOptions },
                },
              ],
            },
            {
              test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
              loader: 'file-loader',
            },
        ]
    },

    optimization: {
        minimizer: [new UglifyJSPlugin(), new OptimizeCssAssetsPlugin()]
    },

    plugins: [
        new MiniCssExtractPlugin({
            filename: config.dev.includes('dev') ? '[name].css' : '[name].min.css',
            chunkFilename: '[id].css',
        }),
        new BrowserSyncPlugin({
            files: [
                '**/*.php',
                'dist/**/*.js',
                'src/**/*.scss'
            ],
            host: 'localhost',
            port: config.port,
            proxy: `http://localhost:${config.proxy}/`
        }, {
            reload: false
        })
    ],

};