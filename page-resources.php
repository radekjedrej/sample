<?php

get_header();

    $perPage = -1;
    $order = 'ASC';

    while ( have_posts() ) : the_post();

        fuzion_layout('builder_banners');

        $resources = new WP_Query( array(
            'post_type' => 'resources',
            'post_status' => 'publish',
            'posts_per_page' => $perPage, 
            'orderby' => 'title', 
            'order' => $order,
        )); 

        $resource_filters = array(
            'brochure' => [],
            'product' => [],
        );

        function resource_field( $name ) {
            return is_array(get_field($name)) 
                ? get_field($name)['label']
                : get_field($name)
            ;
        }

        function resource_checkbox( $type, $name ) {
            $html = '<li><input type="checkbox" id="filter-'. $name .'" class="fuzion-filter__checkbox" data-type="'. $type .'" data-name="'. $name .'">';
            $html .= '<label class="fuzion-options__label " for="filter-'. $name .'">'. $name .'</label>';
            $html .= '</li>';

            return $html;
        }

        ?>
        <section class="space-t--lg space-b--lg" data-grid data-flex="col reverse">

        <article class="resources-list">

        <?php while ( $resources->have_posts() ) : $resources->the_post(); 
            $type_brochure = resource_field('brochure_type');
            $type_product = resource_field('product_type');
        ?>

        <a href="<?= get_field('file')['url'] ?>" download class="resource hover-link" data-flex="row keep justify" <?php 
        ?>data-name="<?= strtolower(get_the_title()) ?>" data-brochure="<?= sanitize_text_field($type_brochure) ?>" data-product="<?= sanitize_text_field($type_product) ?>">
            <p class="resource__info resource__info--brochure"><?= $type_brochure ?></p>
            <h2 class="resource__head hover-link__color section-title"><?php the_title() ?></h2>
            <p class="resource__info resource__info--product"><?= $type_product ?></p>
            <p class="resource__info resource__info--collection"><?= get_field('collection_name') ?></p>
            <?php include get_icons_directory('b-next.svg') ?>
        </a>
        <?php 

        // Restore filter options
        if ( ! in_array( $type_brochure, $resource_filters['brochure'] ))
            array_push( $resource_filters['brochure'], $type_brochure );
            if ( ! in_array( $type_product, $resource_filters['product'] )) 
                array_push( $resource_filters['product'], $type_product );

        endwhile; 
        wp_reset_postdata(); 

        $title_nofound = __('No resources found.', 'fuzion');
        echo "<div id=noresources class='d-none'><h2 class='section-title space-t--lg'>{$title_nofound}</h2></div>";
        
        echo "</article><!-- .resources--list -->";
        
        ?>

        <header class="resources-filters hgroup" data-flex="row justify">
            <div class="resources-filters__head">
                <h1 class="section-title"><?= get_the_title() ?></h1>
                <div class="fuzion-nav__trigger-wrap" data-flex>
                    <a class="js-tabs-filter tabs-product__control"><?php include get_icons_directory('i-filter.svg') ?><span class="tabs-filter__title section-title">Filter</span></a>
                    
                    <?php fuzion_inner_search_form() ?>
                </div>
            </div>

            <div class="fuzion-filters fuzion-filters--dropdowns fuzion-filters--static">
                
                <?php fuzion_inner_search_form() ?>

                <ul class="woof_redraw_zone woof_sid_auto_shortcode list js-tabs_ul">
                    <li class="fuzion-options">
                        <div class="fuzion-options__drop">
                            <h4 class><?= __('Brochure Type', 'fuzion') ?></h4>
                            <div class="woof_block_html_items">
                                <ul class="fuzion-options__list">
                                <?php foreach ( $resource_filters['brochure'] as $label ) 
                                    echo resource_checkbox( 'brochure', $label );
                                ?>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="fuzion-options">
                        <div class="fuzion-options__drop">
                            <h4 class><?= __('Product Type', 'fuzion') ?></h4>
                            <div class="woof_block_html_items">
                                <ul class="fuzion-options__list">
                                <?php foreach ( $resource_filters['product'] as $label ) 
                                    echo resource_checkbox( 'product', $label );
                                ?>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

        </header>

        <?php
        
        echo "</section>";

    endwhile; 
    
do_action( 'storefront_sidebar' );
get_footer();
