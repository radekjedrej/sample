<?php 
$title = get_sub_field("title") ? get_sub_field("title") : "";
$subtitle = get_sub_field("subtitle") ? get_sub_field("subtitle") : "";
?>

<section class="contact-addresses" data-grid=top>
  <header class="hero hgroup--top">
    <h1 class="hero__title fuzion-title space-b--lg"><?= $title ?></h1>
    <div>
      <p class="text-brown"><?= $subtitle ?></p>
    </div>
  </header>
  <aside class="addresses space-t--xl" data-flex="row">
  <?php   
  if (have_rows("addresses")) :
    while (have_rows("addresses")) : the_row();  
    
    $title = get_sub_field("title") ? get_sub_field("title") : "";
    $text = get_sub_field("text") ? get_sub_field("text") : "";

  ?>

    <article class="addresses__box">
      <h2 class="font-normal space-b--xs"><?= $title ?></h2>
      <p class="addresses__copy text-brown"><?= $text ?></p>
    </article>

  <?php
    endwhile;
  endif; 
  ?>
  </aside>
</section> 

