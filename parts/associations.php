<?php
$title = get_sub_field("title") ? get_sub_field("title") : "";
?>

<section data-grid>
  <div class="associations">
    <div class="associations__row associations__title">
      <h3 class="section-title"><?= $title ?></h3>
    </div>

    <?php
    if (have_rows("associations")) :
      while (have_rows("associations")) : the_row();  
      
      $name = get_sub_field("title") ? get_sub_field("title") : "";
      $href = get_sub_field("href") ? get_sub_field("href") : ""; ?>

      <div class="associations__row">
        <a href="<?= $href ?>" class="section-title"><?= $name ?></a>
      </div>

    <?php
      endwhile;
    endif; 
    ?>
  </div>
</section>