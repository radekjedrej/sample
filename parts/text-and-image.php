<?php

$title = get_sub_field('title');
$text = get_sub_field('text');

$logos = get_sub_field('logos');
$img = get_sub_field('image');
$bg = get_sub_field('background_color');

$is_right = get_sub_field('image_on_right');
?>

<section class="section-image" style="--bg: <?= $bg ?>">

    <div data-grid data-flex="row justify<?php if ($is_right) echo ' reverse' ?>">

        <picture class="section__image">
            <?= wp_get_attachment_image( $img['id'], 'half' ) ?>
        </picture>
        
        <div class="section__text">
            <h1 class="section-title space-b"><?= $title ?></h1>
            <p><?= $text ?></p>
        </div>

    </div>

</section>