<main data-grid="top">
    
    <header class="chapter__careers">
        <p>Careers</p>
    </header>

    <?php   
    if (have_rows("careers")) :
      while (have_rows("careers")) : the_row(); 

      $title = get_sub_field("title") ? get_sub_field("title") : "";
      $description = get_sub_field("description") ? get_sub_field("description") : "";
      $href = get_sub_field("href") ? get_sub_field("href") : "";
      
    ?>

    <div class="accordion__row accordion__row--careers" data-flex>
        <div class="accordion__question">
            <h2 class="js-accordion-button fuzion-title" data-flex="row keep center justify">
                <p><?= $title ?></p>
                <button class="plus plus--mobile"><?php fuzion_reveal_button() ?></button>
            </h2>
        </div>

        <div class="js-answer accordion__answer">

            <div class="accordion__careers-descritpion">
                <p><?= $description ?></p>
            </div>

            <div class="accordion__careers--link"><?php 
                fuzion_cta_link( $href, 'Apply', false ) 
            ?></div>

        </div>
    </div>

    <?php
        endwhile;
    endif; 
    ?>

</main>