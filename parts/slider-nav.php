<?php $class = get_query_var('slider_class') ?>
<div class="slide-nav<?= $class ? " slide-nav--{$class}" : ' slide-nav--home' ?>" data-flex>
    <button aria-label="Previous Carousel Slide" class="slide-nav__btn slide-nav__btn--prev">
    <?php 
        if($class === 'gallery'): 
            include get_icons_directory('i-previous.svg');
        else:
            include get_icons_directory('i-arrow.svg');
        endif; ?>
    </button>
    <button aria-label="Next Carousel Slide" class="slide-nav__btn slide-nav__btn--next">
    <?php 
        if($class === 'gallery'): 
            include get_icons_directory('i-previous.svg');
        else:
            include get_icons_directory('i-arrow.svg');
        endif; ?>
    </button>
    </button>
</div>