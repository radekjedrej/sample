<?php 

$carousel = ($items = get_query_var('slider_items')) ? $items : get_sub_field('carousel');
$carouselTitle = ($title = get_query_var('slider_title')) ? $title : get_sub_field('main_title');
$carouselClass = is_singular('projects') ? ' slider--gallery slider--gallery-projects' : ' slider--gallery';

echo "<section class='slider{$carouselClass}'>";

if ($carouselTitle) echo "<h1 class='fuzion-title'>{$carouselTitle}</h1>";

echo '<div class="slide-wrap" data-slider="gallery" data-flex>';

foreach ( $carousel as $x => $links ) :
    $title = $links['title'];
    $description = $links['description'];
    $text = $links['text'];
    $client = $links['client'];
    $product = $links['product'];

    $cta = [
        'text' => $links['cta_text'],
        'href' => $links['cta_href']
    ];

    $img = $links['image'];

    ?>
    <article class="slide" data-flex="col">
        
        <picture>
            <?= wp_get_attachment_image( $img['id'], 'hd' ) ?>
        </picture>

        <header class="slide__info">
            <h2 class="section-title space-b--xs"><?= $title ?></h2>
            <?php if ($titleImage) : ?>
            <h2 class="section-title text-white"><?= $titleImage ?></h2>
            <a class="btn--cta text-white" href="<?= $cta['href'] ?>"><?= __('Discover', 'fuzion') ?></a>
            <?php endif;
            
                fuzion_conditional_text($client);
                fuzion_conditional_text($product);
            
            if ($cta['text']) : ?>
            <a class="btn--cta" href="<?= $cta['href'] ?>"><?= $cta['text'] ?></a>
            <?php endif ?>
        </header>
        
    </article>
    <?php 
endforeach;

echo '</div>';

set_query_var('slider_class', 'gallery');
get_template_part('parts/slider', 'nav');

echo '</section><!-- .slider--gallery -->';
?>