<?php 

$quickLinks = get_sub_field('list_quick_links');
$mobile_accordion = get_sub_field('mobile_accordion') ? 'site-link__accordion' : 'site-remove__accordion';
$quickLinksHtml = '';
$overlay = get_field('overlay', 'options') ? get_field('overlay', 'options') : "";
$opacity = get_field('opacity', 'options') ? get_field('opacity', 'options') : 0;
$opacity = $opacity / 100;


if ($quickLinks) :
    
    $quickLinksHtml = '<ul class="site-links list list--block">';
    foreach ($quickLinks as $links) :
        $title = $links['title'];
        $href = $links['cta_href'];

        $quickLinksHtml .= "<li><a class='site-links__item' href='{$href}'>{$title}</a></li>";
    endforeach;
    $quickLinksHtml .= '</ul>';


echo "<section class='slider slider--double {$mobile_accordion}'>" .
     '  <article class="slide"  data-flex="row justify">' .
     '      <div class="slider--double__text" data-flex="col">' .
     '      <div data-slider="slideText" data-flex="row keep">';

if( $quickLinks) :
foreach ( $quickLinks as $x => $links ) :
    $url = $links['cta_href'];
    $title = $links['title'];
    $subtitle = $links['subtitle'];
    $text = $links['text'];
    ?>
    <div class="slide__text" data-flex="col" data-a="<?= ($x + 1) ?>">
        <h2 class="section-caption space-t--lg space-b"><?= $subtitle ?></h2>
        <h1 class="section-title space-b"><?= $title ?></h1>

        <p class="space-b"><?= $text ?></p>

        <?php if( $url ): ?>
        <div class="space-b"><?php 
            fuzion_cta_link( $links['cta_href'] ) 
        ?></div>
        <?php endif ?>
    </div>
    <?php 
endforeach;
endif;

echo '      </div><!-- .slide__text -->' .
     '  <div class="space-b">' . fuzion_cta_link( $quickLinks[0]['cta_href'] ) . '</div>' . $quickLinksHtml .
     '      </div><!-- .slider--double__text -->' .
     '      <div data-slider="slideImage" data-flex="row keep">';

if( $quickLinks) :
foreach ( $quickLinks as $x => $links ) :
    $url = $links['cta_href'];
    $title = $links['title'];
    $subtitle = $links['subtitle'];
    $images = [
        'front' => wp_is_mobile() ? $links['front_image_mobile'] : $links['front_image'],
        'back' => $links['back_image']
    ];

    
    ?>
    <div class="slide__cover" style="<?= $images['back'] ? bg_cover($images['back']) : '' ?>" data-flex="center">
        <?= wp_get_attachment_image( $images['front']['id'], 'half' ) ?>
        <div class="mobile-only">
            <h2 class="section-caption space-b"><?= $subtitle ?></h2>
            <h1 class="section-title"><?= $title ?></h1>
        </div>
        <?php if( $url ): ?>
            <a class="slide__cover__link" href="<?= $url ?>"></a>
        <?php endif ?>
        <?php if( $overlay ): ?>
            <div class="slide-overlay slide-overlay--mobile" style="background-color: rgba(0,0,0,<?= $opacity ?>);"></div>
        <?php endif; ?>
    </div>
    <?php 

endforeach;
endif;

echo '      </div>' .
     '  </article>' .
     '</section><!-- .slider--double -->';
?>

<?php
if ($mobile_accordion === 'site-link__accordion') : ?>

    <div class="tab__content__wrapper <?= $mobile_accordion ?>" data-grid>
        <div class="tab__content open" id="1">

        <?php
        foreach ($quickLinks as $links) :
            $title = $links['title'];
            $text = $links['text'];
        ?>

            <div class="accordion__row" data-flex="">
                <div class="accordion__question">
                    <h2 class="js-accordion-button section-title" data-flex="row keep center justify">
                        <p><?= $title ?></p>
                        <button class="plus plus--mobile"><?php fuzion_reveal_button() ?></button>
                    </h2>
                </div>
                <div class="js-answer accordion__answer">
                    <p><?= $text ?></p>
                </div>
            </div>

        <?php
        endforeach; ?>

        </div> 
    </div>
<?php
endif;
endif;
?>
