<?php

$object = ($accordion = get_query_var('accordion')) ? $accordion : null;

$title = $object ? $object['title'] : get_sub_field("title");
$text = $object ? $object['text'] : get_sub_field("text");

?>
<div class="accordion__row" data-flex>
    <div class="accordion__question">
        <h2 class="js-accordion-button section-title" data-flex="row keep center justify">
            <p><?php echo $title ?></p>
            <button class="plus plus--mobile"><?php fuzion_reveal_button() ?></button>
        </h2>
    </div>
    <div class="js-answer accordion__answer">
        <p><?php echo $text ?></p>
    </div>
</div>