<nav class="fuzion-nav products-filter" data-flex="col">
	<button aria-label=Close class="btn--clean btn--icon fuzion-nav__close" data-flex="row end">
    <?php include get_icons_directory('i-cross.svg') ?>
	</button>

	<section class="fuzion-nav__pages -vscroll">

		<p class="fusion-filter__heading"><?= __('Filter', 'fuzion') ?></p>

		<ul class="js-fuzion-filter__list tabs_ul fusion-filter--menu">
			<?php echo do_shortcode('[woof by_only="by_onsales"]'); ?>
		</ul>

	</section>

</nav>