<?php
$title = !empty(get_sub_field("title")) ? get_sub_field("title") : "";
$content = !empty(get_sub_field("content")) ? get_sub_field("content") : "";
?>

<div class="wysiwyg" data-grid="top">
    <?php if($title): ?>
        <h1 class="wysiwyg__title"><?= $title ?></h1>
    <?php endif; ?>
    <?php if($content): ?>
        <?= $content ?>
    <?php endif; ?>
</div>