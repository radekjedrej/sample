<nav class="fuzion-nav" data-flex="col">
	<?php fuzion_langs(); ?>
	
	<button aria-label=Close class="btn--clean btn--icon fuzion-nav__close">
    <?php include get_icons_directory('i-cross.svg') ?>
	</button>

	<section class="fuzion-nav__pages">
	<?php 
		fuzion_menu_main('fuzion-menu fuzion-menu--overlay');
		fuzion_menu_pages();
	?>
	</section>

	<footer data-flex="row space center">
	<?php 
		get_template_part('parts/pathway', 'menu');
		fuzion_social_icons();
	?>
	</footer>
</nav>

<div class="fuzion-overlay" aria-hidden="true"></div>