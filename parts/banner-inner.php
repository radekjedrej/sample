<?php 

$banner = ($copy = get_query_var('banner_copy')) ? $copy : null;

?>
<header class="chapter">
    <h1 class="chapter__heading fuzion-title"><?= $banner['title'] ?></h1>
    <div class="chapter__all">
        <a class="btn--link" href="<?= $banner['cta_href'] ?>"><?= 
            $banner['cta_text']
        ?></a>
    </div>
</header>