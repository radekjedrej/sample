<!-- Social share -->
<div class="social-icons--wrapper">

    <div class="social-icons--share">
        <p class="single-share__title"><?= __('Share', 'fuzion') ?></p>

        <?php fuzion_social_icons(true);  ?>
    </div>

</div>