<?php 

$products = !empty(get_sub_field("products")) ? get_sub_field("products") : "";

?>

<div class="js-tabs tabs__wrapper" data-grid>
  <div class="tabs__header" data-flex="row justify">

    <p class="section-title"><?= __('Care & Maintenance', 'fuzion') ?></p>

    <div class="tabs__filter">
      <a class="js-tabs-filter tabs-filter__control">
        <?php include get_icons_directory('i-filter.svg') ?>
        <span class="tabs-filter__title section-title"><?= __('Filter', 'fuzion') ?></span>
      </a>
      
      <ul class="js-tabs--desktop js-tabs_ul tabs_ul"><?php   
      $count = 1;

      if (have_rows("products")) :
        while (have_rows("products")) : the_row();  
        
        $name = !empty(get_sub_field("name")) ? get_sub_field("name") : "";
        $filter_name = strtolower($name);
        $filter_name = str_replace(' ', '-', $filter_name);
      ?>

      <label class="tab-button radio-btn <?php echo $count === 1 ? 'open' : ''  ?>" for="<?php echo $filter_name; ?>" data-flex="row keep center">
        <input type="radio" id="<?php echo $filter_name; ?>" name="filter" data-filter="<?php echo $count; ?>"/>
        <span class="radio-circle"><span class="radio-full"></span></span>
        <p class="radio-btn__name"><?php echo $name; ?></p>
      </label>

      <?php
        $count++;
        endwhile;
      endif; 
      ?>
      </ul>
    </div>

  </div>
  <div class="contentWrapper">
  <?php   
  $count = 1;

  if (have_rows("products")) :
    while (have_rows("products")) : the_row(); ?>
    
    <div class="tab__content <?php echo $count === 1 ? 'open' : ''  ?>" id="<?php echo $count ?>"><?php

      fuzion_layout('one_product')
    
    ?></div>

  <?php
    $count++;
    endwhile;
  endif; 
  ?>
  
  </div>
</div>