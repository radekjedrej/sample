<?php 
$image = get_sub_field("image") ? get_sub_field("image") : "";
$cta_text = get_sub_field("cta_text") ? get_sub_field("cta_text") : "";
$cta_href = get_sub_field("cta_href") ? get_sub_field("cta_href") : "";
$form_title = get_sub_field("form_title") ? get_sub_field("form_title") : "";
$form_subtitle = get_sub_field("form_subtitle") ? get_sub_field("form_subtitle") : "";
$form_shortcode = get_sub_field("form_shortcode") ? get_sub_field("form_shortcode") : "";
?>


<section class="contact-form__wrapper space-b--lg">
    <div class="contact-form__img">
      <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
    </div>

    <div data-grid>

      <div class="contact-form">
        <div class="contact-form__box">
          <?php if (get_sub_field("has_cta")): ?>
          <div class="contact__retailer section-title space-b--lg">
            <?php fuzion_cta_link( $cta_href ? $cta_href : '/', $cta_text, true ) ?>
          </div>
          <?php endif ?>
          <header>
            <h1 class="section-title space-b--xs"><?= $form_title ?></h1>
            <div class="text-brown contact__subtitle"><?= $form_subtitle ?></div>
          </header>
          <?= do_shortcode($form_shortcode) ?>
        </div>
      </div>

    </div>

</section>