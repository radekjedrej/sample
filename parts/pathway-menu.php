<ul class="fuzion-nav__cta list">
    <li><a href="<?= get_landing_link('professional') ?>"><?= 
        __('Fuzion for Professionals', 'fuzion') 
    ?></a></li>
    <li><a href="<?= get_landing_link() ?>"><?= 
        __('Fuzion for Homeowners', 'fuzion') 
    ?></a></li>
</ul>