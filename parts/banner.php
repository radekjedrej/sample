<?php 
$overlay = get_field('overlay', 'options') ? get_field('overlay', 'options') : "";
$opacity = get_field('opacity', 'options') ? get_field('opacity', 'options') : 0;
$opacity = $opacity / 100;

$carousel = ($items = get_query_var('slider_items')) ? $items : get_sub_field('carousel');
$menu = ($aside = get_query_var('slider_aside')) ? $aside : null;
$is_aside = count($menu);
$is_front = is_front_page();
$is_single = 2 > count($carousel);

?>
<section class="section-white<?php if ( $is_front ) echo ' section--home' ?> slider slider--full<?php if ( $is_aside ) echo ' slider--aside' ?>">
    
    <div class="slide-wrap" data-flex data-slider="home">
        <?php foreach ( $carousel as $x => $slide ) : 
            
            $image = wp_is_mobile() ? $slide['background_image_mobile'] : $slide['background_image'];
            $image = $image ? $image : $slide['background_image'];
            $subtitle = $slide['subtitle'] ? $slide['subtitle'] : $slide['index'];
        ?>

        <article class="slide tns-lazy-img" data-flex="col" style="<?= bg_cover($image) ?>">
            <div class="slide-content">
                <h1 class="fuzion-title space-b--lg"><?= 
                    $slide['title'] 
                ?></h1>
                <div class="slide__text" data-x="<?= !$is_single ? 0 . trim($x + 1) : $subtitle ?>">
                    <p><?= 
                        $slide['text'] 
                    ?></p>
                </div>
            </div>

            <?php if($slide['discover_link']->ID): ?>
                <div class="slide__link">
                    <a class="btn--small" href="<?php the_permalink( $slide['discover_link']->ID ) ?>"><?= __('DISCOVER', 'fuzion') ?>+</a>
                </div>
            <?php endif; ?>
            
            <?php if($overlay): ?>
                <div class="slide-overlay" style="background-color: rgba(0,0,0,<?= $opacity ?>);"></div>
            <?php endif; ?>
            
        </article>
        <?php endforeach ?>
    </div>
    <?php 
    if ( !$is_single ) 
        get_template_part('parts/slider', 'nav');

    if ( $is_aside ) :
        echo '<nav class="site-links-wrap" data-flex=col>';
        fuzion_site_links( $menu, __('Support Items', 'fuzion') );
        echo '</div>';
    endif;

    if ( $is_front )
        get_template_part('parts/pathway', 'home');
    ?>
</section>