<?php 

$name = !empty(get_sub_field("name")) ? get_sub_field("name") : "";
$text = !empty(get_sub_field("text")) ? get_sub_field("text") : "";
$image = !empty(get_sub_field("image")) ? get_sub_field("image") : "";
$client = !empty(get_sub_field("client")) ? get_sub_field("client") : "";
$product = !empty(get_sub_field("product")) ? get_sub_field("product") : "";
$project_spec = !empty(get_sub_field("project_spec")) ? get_sub_field("project_spec") : "";
?>

<section class="project-banner__wrapper">
  <div class="project-banner" data-flex="row">
    <div class="project-banner__copy" data-flex="column justify">
      <div>
        <a class="project-banner__link btn--link" href="<?= esc_url(site_url('/projects')); ?>"><?= __('Back to all Projects', 'fuzion') ?></a>
      </div>
      <div class="project-banner_box">
        <h1 class="project-banner__title font-normal"><?= $name ?></h1>
        <p class="project-banner__text"><?= $text ?></p>
      </div>
    </div>
    <div class="project-banner__media">
      <img class="project-banner__img" src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
      <div class="project-banner__info">
      <?php if($product): ?>
        <div class="project-banner__box text-white">
          <p><?= __('Product:', 'fuzion') ?></p>
          <p><?= $product ?></p>
        </div>
        <?php endif; ?>
        <?php if($client): ?>
        <div class="project-banner__box text-white">
          <p><?= __('Client:', 'fuzion') ?></p>
          <p><?= $client ?></p>
        </div>
        <?php endif; ?>
        <?php if($project_spec): ?>
        <div class="project-banner__box text-white">
          <p><?= __('Project Spec:', 'fuzion') ?></p>
          <p><?= $project_spec ?></p>
        </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>