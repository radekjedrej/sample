<?php

$title = get_sub_field('title');
$text = get_sub_field('text');
$link = get_sub_field('cta_href');

$img = [
    'left' => get_sub_field('left_image'),
    'middle' => get_sub_field('middle_image'),
    'right' => get_sub_field('right_image')
];

?>
<section class="lookbook space-t--xl" data-flex="row">

    <div class="lookbook__img" style="<?= bg_cover( $img['left'] ) ?>">
    </div>

    <article class="lookbook__copy" data-flex="col">
        <header>
            <h1 class="section-title"><?= $title ?></h1>
            <?php fuzion_cta_link( $link, $text, false ) ?>
        </header>

        <?= wp_get_attachment_image( $img['middle']['id'], 'half' ) ?>
    </article>

    <div class="lookbook__img" style="<?= bg_cover( $img['right'] ) ?>">
    </div>

</section>