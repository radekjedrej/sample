<?php

$projects = get_sub_field("projects") ? get_sub_field("projects") : "";

if($projects):

  $sliderTitle = __('Recent Industry Work', 'fuzion');
  $sliderItems = array();

  foreach ($projects as $item) :

    $project_id = $item->ID;
    $title = $item->post_title;
    $link = get_permalink($project_id);
    $custom_fields = get_field( 'builder_banners', $project_id );
    $image_id = $custom_fields[0]['image']['ID'];
    $client = $custom_fields[0]['client'];
    $product = $custom_fields[0]['product'];

    array_push($sliderItems, array(
      'title' => $title,
      'image' => ['id' => $image_id],
      'client' => $client,
      'product' => $product,
			'cta_text' => 'View Project',
			'cta_href' => $link
    ));

  endforeach;

  set_query_var('slider_modifier', 'slide__info--project');
  set_query_var('slider_title', $sliderTitle);
  set_query_var('slider_items', $sliderItems);
  get_template_part('parts/image-carousel', 'nav');

endif;


