<nav class="fuzion-nav fuzion-filter" data-flex="col">
	<button aria-label=Close class="btn--clean btn--icon fuzion-nav__close" data-flex="row end">
    <?php include get_icons_directory('i-cross.svg') ?>
	</button>

	<section class="fuzion-nav__pages">
		<p class="fusion-filter__heading"><?= __('Filter', 'fuzion') ?></p>

		<ul class="js-fuzion-filter__list js-tabs--mobile fusion-filter--menu"></ul>
	</section>
</nav>