<?php

$links = get_sub_field('links');
$img = get_sub_field('background_image');

$cta = [
    'text' => get_sub_field('cta_text'),
    'href' => get_sub_field('cta_href')
];

?>
<section class="fuzion-pathway-section">

    <picture>
        <?= wp_get_attachment_image( $img['id'], 'hd' ) ?>
    </picture>

    <ul class="fuzion-pathway list">
        <?php foreach ( $links as $x => $link ) : ?>
        <li class="fuzion-pathway__cta<?= $link['large'] ? ' -large' : '' ?>" style="--bg: <?= $link['background_color'] ?>" data-flex>
            <a href="<?= $link['cta_href'] ?>" data-flex><?= $link['text'] ?></a>
        </li>
        <?php endforeach ?>
    </ul>

</section>