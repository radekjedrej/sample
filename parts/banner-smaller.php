<?php 

$pageParent = wp_get_post_parent_id(
    get_queried_object_id()
);

$supportItems = get_field('support_items');

$sliderText = $pageParent ? get_the_title( $pageParent ) : '';
$sliderItems = array(
    array(
        'index' => $sliderText,
        'title' => get_sub_field('title'),
        'subtitle' => get_sub_field('subtitle'),
        'text' => get_sub_field('text'),
        'background_image' => get_sub_field('image')
    )
);

set_query_var('slider_items', $sliderItems);
set_query_var('slider_aside', $supportItems);
get_template_part('parts/banner');