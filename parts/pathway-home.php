<nav class="fuzion-pathway" data-flex="col">
    <a class="fuzion-pathway__cta" data-flex href="<?= get_landing_link('pro') ?>">
        <span><?= 
            __('Professional', 'fuzion') 
        ?></span>
    </a>
    <a class="fuzion-pathway__cta" data-flex href="<?= get_landing_link() ?>">
        <span><?= 
            __('Homeowner', 'fuzion') 
        ?></span>
    </a>
</nav>