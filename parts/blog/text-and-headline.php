<?php
$title = get_sub_field("title") ? get_sub_field("title") : "";
$text = get_sub_field("text") ? get_sub_field("text") : "";


?>

<section data-grid=wide>
  <div class="blog-content" data-flex>
    <div class="blog-content__title">
      <span class="blog_date text-brown"><?= get_the_date( 'd/m/y' ); ?></span>
      <h2 class="section-title"><?= $title ?></h2>
    </div>
    <div class="blog-content__copy text-brown">
      <p><?= $text ?></p>
    </div>
  </div>
</section>