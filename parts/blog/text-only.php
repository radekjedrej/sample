<?php
$text = get_sub_field("text") ? get_sub_field("text") : "";

?>

<section class="single-text" data-grid=wide>
  <div class="blog-content__copy blog-content__copy--right">
    <p class="text-brown"><?= $text ?></p>
  </div>
</section>
