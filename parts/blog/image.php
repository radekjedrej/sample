<?php
$image = get_sub_field("image") ? get_sub_field("image") : "";

?>

<section class="single-image" data-grid=wide>
  <picture>
    <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
  </picture>
</section>