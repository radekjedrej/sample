<?php

$repeater = get_sub_field("collections") ? get_sub_field("collections") : "";

if($repeater):

  $sliderTitle = __('Favored Industry Collections', 'fuzion');
  $sliderItems = array();

  foreach ($repeater as $item) :

    $link = get_term_link( $item );
    $title = get_term( $item )->name;
    $custom_fields = get_field( 'builder_banners', $item);
    $image_id = $custom_fields[0]['image']['ID'];

    array_push($sliderItems, array(
      'title' => $title,
      'image' => ['id' => $image_id],
			'cta_text' => 'View Collection',
			'cta_href' => $link
    ));

  endforeach;

  set_query_var('slider_modifier', '');
  set_query_var('slider_title', $sliderTitle);
  set_query_var('slider_items', $sliderItems);
  get_template_part('parts/image-carousel', 'nav');

endif;


