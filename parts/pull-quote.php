<?php 

$subtitle = get_sub_field('pre_title');
$text = get_sub_field('text');
$author = get_sub_field('author_name');
$position = get_sub_field('position_name');

$cta = [
    'text' => get_sub_field('cta_text'),
    'href' => get_sub_field('cta_href')
];

$is_dark = get_sub_field('dark_layout');
$is_testimonial = $author && $position;

?>

<?php if($text): ?>
<section class="quote<?= $is_dark ? ' quote--dark' : '' ?>">
    <blockquote data-grid>
        
        <div class="quote__text" data-pre="<?= $subtitle ?>"><?= 
            $text 
        ?></div>

        <footer class="quote__footer">
        <?php if ( $is_testimonial ): ?>
        <p class=vcard data-flex="col">
            <span class=fn><?= $author ?></span>
            <span class=org><?= $position ?></span>
        </p>
        <?php elseif ( $cta['href'] ): ?>
        <p>
            <a href="<?= $cta['href'] ?>"><?= $cta['text'] ?></a>
        </p>
        <?php endif ?>
        </footer>

    </blockquote>
</section>
<?php endif; ?>
