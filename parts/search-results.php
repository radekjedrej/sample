<?php 
$results = ($items = get_query_var('search_items')) ? $items : null;
?>

<?php if($results):

  foreach ( $results as $x => $result ) :

    $title = $result["title"];
    $cta_href = $result["cta_href"];
    $img_id = $result["img_id"];
    $post_type = $result["post_type"];
  ?>

  <a class="hover-link" href="<?= $cta_href ?>">

    <div class="search__content">

      <div class="search__header" data-flex>
        <h2 class="section-title hover-link--color"><?= $title ?></h2>
        <p class="search__category"><?= $post_type ?></p>
      </div>

      <div class="search__image <?= $img_id  ? "" : "search__image--svg" ?>">
        <picture>
            <?php 
            if($img_id):
              echo wp_get_attachment_image( $img_id, 'half' );
            else:
              include get_icons_directory('logo.svg');
            endif ?>
        </picture>
      </div>

    </div>

  </a> 

  <?php
  endforeach;  
  ?>

<?php endif; ?>