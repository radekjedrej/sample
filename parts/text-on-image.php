<?php

$text = get_sub_field('text');
$img = get_sub_field('image');

$cta = [
    'text' => get_sub_field('cta_text'),
    'href' => get_sub_field('cta_href')
];

?>

<section class="section-cta" data-flex style="<?= bg_cover($img) ?>">
    
    <header class="section__text" style="--color-bg: rgba(255,255,255, .75);--color-fg: rgba(255,255,255, 1);">
        <div class="fuzion-title space-b">
            <?= $text ?>
        </div>
        <?php 
            fuzion_cta_link( $cta['href'] ? $cta['href'] : '/', $cta['text'], false )
        ?>
    </header>

</section>