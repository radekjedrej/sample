<?php
$spacing_value = !empty(get_sub_field("spacing_value")) ? get_sub_field("spacing_value") : "";
?>

<div class="space-<?= $spacing_value; ?>"></div>
