<?php
$title = !empty(get_sub_field("title")) ? get_sub_field("title") : "";
$text = !empty(get_sub_field("text")) ? get_sub_field("text") : "";
?>

<div class="accordion__row" data-flex>
  <article class="accordion__question">
    <h2 class="js-accordion-button section-title font-normal" data-flex="row keep center justify">
      <p><?php echo $title ?></p>
      <button class="plus plus--mobile"><?php fuzion_reveal_button() ?></button>
    </h2>
  </article>
  <div class="js-answer accordion__answer">
    <p class="space-b"><?php echo $text ?></p>
    <?php   
    if (have_rows("sliders")) :
      while (have_rows("sliders")) : the_row();  

      $product = array(
        'image' => get_sub_field("image"),
        'title' => get_sub_field("title"),
        'text' => get_sub_field("text"),
        'cta_href' => get_sub_field("cta_link"),
        'cta_text' => get_sub_field("cta_text"),
      );

      fuzion_product_related($product);

      endwhile;
    endif; 
    ?>
  </div>
</div>