<?php
$title = get_sub_field("title") ? get_sub_field("title") : "";
$subtitle = get_sub_field("subtitle") ? get_sub_field("subtitle") : "";
$text = get_sub_field("text") ? get_sub_field("text") : "";
$cta_text = get_sub_field("cta_text") ? get_sub_field("cta_text") : "";
$cta_href = get_sub_field("cta_href") ? get_sub_field("cta_href") : "";
$image = get_sub_field("image") ? get_sub_field("image") : "";
?>

<section class="header-margin-top" data-grid="top">
  <div class="section-image section-image--border section-visualize">
    <div class="visualize__copy section__text section__text--auto" data-flex="column justify">
      <div class="visualize__box" data-flex="column justify">
        <div>
          <h1 class="fuzion-title"><?= $title ?></h1>
        </div>
        <div>
          <h1 class="visualize__title font-normal"><?= $subtitle ?></h1>
          <p class="visualize__text text-brown"><?= $text ?></p>
          <?php /* <a href="<?= $cta_href ?>" class="visualize__text"><?= $cta_text ?></a> */ ?>
          <span class="visualize__text"><a href="javascript: roomvo.startStandaloneVisualizer();"><?= $cta_text ?></a></span>
        </div>
      </div> 
    </div>
    <div class="visualize__media section__image" data-flex="col">
      <p class="fuzion-title"><?= __('Visualize Your Room', 'fuzion') ?></p>
      <img class="visualize__img" src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
    </div>
  </div>
</section>