<?php
/**
 * The template for displaying the homepage.
 *
 * Template name: Journal Archive
 *
 * @package storefront
 */

get_header();

    while ( have_posts() ) :
        the_post();

        fuzion_layout('builder_banners');

        echo '<section class="journal__carousel">'; // Carousel (journal__carousel) Start!

        $carouselPosts = new WP_Query( array(  
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => 4, 
            'orderby' => array(
            'date' =>'DESC',
            )
        )); 

        if ($carouselPosts->have_posts()):

            $sliderTitle = __('Journal', 'fuzion');
            $sliderModifier = "slider--basic";
            $sliderItems = array();

            while($carouselPosts->have_posts()):
            $carouselPosts->the_post();

                $custom_fields = get_post_custom(get_the_ID());
                $image = $custom_fields['builder_blog_1_image'][0] ? $custom_fields['builder_blog_1_image'][0] : '';
                $sliderImage = get_post_thumbnail_id(get_the_ID()) ? get_post_thumbnail_id(get_the_ID()) : $image;

                array_push($sliderItems, array(
                    'titleImage' => get_the_title(),
                    'image' => ['id' => $sliderImage],
                    'cta_href' => get_permalink()
                ));

            endwhile;
            endif;

            set_query_var('slider_modifier', $sliderModifier);
            set_query_var('slider_title', $sliderTitle);
            set_query_var('slider_items', $sliderItems);
            get_template_part('parts/image-carousel', 'nav');

            wp_reset_postdata();

        echo '</section>'; // Carousel (journal__carousel) End! ?>

        
        <section class="journal__blog tabs__wrapper" data-grid> <!-- Blog (journal__blog) Start! --> 
            <header class="tabs__header" data-flex="row justify">

                <p class="section-title"><?= __('Journal', 'fuzion') ?></p>
                <div class="tabs__filter">
                
                    <ul class="js-tabs--journal js-tabs_ul tabs_ul"><?php

                    $cat_terms = get_terms(
                        array('category'),
                        array(
                            'hide_empty'    => true,
                            'orderby'       => 'name',
                            'order'         => 'ASC',
                            'number'        => 50
                        )
                    );

                    $count = 1;
                    
                    if( $cat_terms ) :
                        foreach( $cat_terms as $term ) : ?>
                    
                            <label class="tab-button radio-btn <?php echo $count === 1 ? 'open' : ''  ?>" for="<?php echo strtolower($term->name); ?>" data-flex="row keep center">
                                <input type="radio" id="<?php echo strtolower($term->name); ?>" name="filter" data-filter="<?php echo strtolower($term->name); ?>"/>
                                <span class="radio-circle"><span class="radio-full"></span></span>
                                <p class="radio-btn__name"><?php echo $term->name; ?></p>
                            </label>
                    
                        <?php   
                        wp_reset_postdata(); //important

                        $count++;
                        endforeach;
                    endif;
                    ?>

                    </ul>
                </div>
            </header>

            <div class="contentWrapper">
                
                <div class="journals columns-3 open">

                <?php
                $blogPosts = new WP_Query( array(  
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'posts_per_page' => -1, 
                    'orderby' => array(
                    'date' =>'DESC',
                    )
                )); 

                if ($blogPosts->have_posts()):
                    while($blogPosts->have_posts()):
                    $blogPosts->the_post(); 
                    
                    $post_date = get_the_date( 'd/m/y' );
                    $categories = get_the_category();

                    $custom_fields = get_post_custom(get_the_ID());
                    $image = $custom_fields['builder_blog_1_image'][0] ? $custom_fields['builder_blog_1_image'][0] : '';
                    $sliderImage = get_post_thumbnail_id(get_the_ID()) ? get_post_thumbnail_id(get_the_ID()) : $image; ?>
                    
                    <article class="journal__list" data-category="<?php foreach($categories as $category): echo (strtolower($category->name) . ' '); endforeach; ?>">
                        <a href="<?= the_permalink(); ?>">
                            <picture>
                                <?= wp_get_attachment_image( $sliderImage, 'half' ) ?>
                            </picture>
                            <header class="journal__head">
                                <div class="journal__details text-brown">
                                    <span><?= $post_date ?></span>
                                    <span><?php 
                                        $count = 0;
                                        $countLength = count($categories);
                                        foreach($categories as $category):
                                            echo (strtoupper($category->name));
                                            if(!$count == $countLength - 1) {
                                                echo(' / ');
                                            }
                                            $count++;
                                        endforeach;
                                    ?></span>
                                </div>
                                <h2 class="section-title"><?= the_title() ?></h2>
                            </header>
                        </a>
                    </article>

                    <?php
                    endwhile;
                endif;
                ?> 

                </div>
            </div>
        </section> <!-- Blog (journal__blog) End! -->

        <?php

    endwhile; 
    
do_action( 'storefront_sidebar' );
get_footer();
