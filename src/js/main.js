import $$ from './lib/_q'
import { Accordion, BgChange, ToggleClass } from './lib/_c'

import { Menus } from './ui/header'
import { Sliders } from './ui/slider'
import Search from './ui/search'

import { AjaxLoadMore } from './ui/ajaxLoadMore'

window.addEventListener('DOMContentLoaded', () => {
    const loadMore = $$('.js-load-more')
    const pullQuote = $$('.quote')
    const searchButton = $$('.js-search-button')
    const message = $$('.limit-message')
    const selectBox = $$('#customerType')

    // $$('#customerType').classList.add('select2-selection', 'select2-selection--single')

    new Menus()
    Accordion()

    if (loadMore) 
        new AjaxLoadMore(loadMore);

    if (searchButton) {
        const formPopup = $$('.fuzion-search')

        new Search(
            searchButton, formPopup
        )
    }

    if (pullQuote) 
        new BgChange(pullQuote);

    if (message)
        new ToggleClass(message, ['show'])

    if (selectBox)
        new ToggleClass(selectBox, ["select2-selection", "select2-selection--single"])

})