import $$ from '../lib/_q'
import { childOf, handleHeight } from '../lib/_c'

import SideMenu from '../lib/sideMenu'
import DeviceCheck from '../lib/deviceCheck'

const isLaptop = new DeviceCheck('laptop')

const checkActive = el => el.classList.contains('active')
const checkReady = (el, trigger) => !trigger || trigger !== el

const removeHeight = (els, trigger = null) => {
    
    // Check whether any's opened
    if ( ! els.some(checkActive) ) return;
    
    els // Close any other opened
        .filter(el => checkReady(el, trigger) && checkActive(el))
        .forEach(el => handleHeight(el))
}

export const ImageSwap = ((image) => {

    image.onclick = () => {
        const imageSide = $$(".js-product__side-img");
        const imageMain = $$("#tns1-item0 img");

        const [ 
            { src: sideSrc }, 
            { src: mainSrc },
        ] = [
            $$(".js-product__side-img"),
            $$("#tns1-item0 img")
        ]

        const images = [imageSide, imageMain]

        imageSide.src = mainSrc;
        imageMain.src = sideSrc;

        images.forEach( el => el.classList.add("product--fade-in") )

        setTimeout(function() {
            images.forEach( el => el.classList.remove("product--fade-in") )
        }, 300)

    }
})

export class ProductFilters {

    constructor( filters, init = true ) {
        this.filters = $$(filters, !0)

        if ( !this.filters ) return;
        
        this.initAll( this.filters, init )
    }

    get filterSelectors() {
        return [...this.filters]
            .map(el => el.querySelector('h4'))
            .filter(el => el !== null)
    }

    fakeButton() {
        const button = document.createElement("button");
        // const srOnly = document.createElement("div");
        // const textNode = document.createTextNode("Reveal filters");

        button.classList.add('plus')
        // srOnly.classList.add('sr-only')
        // srOnly.classList.add('sr-only-focusable')
        // srOnly.appendChild(textNode); 

        this.button = button
    }
    
    setDropdownIcons() {
        this.fakeButton(); 
        
        this.filterSelectors.forEach(el => {
            el.appendChild( this.button.cloneNode(true) )
        })
    }

    setDropdowns() {
        const { filterSelectors } = this
        const { body } = document

        // Hide dropdown(s) on body click
        body.onclick = ({ target }) => {

            if ( filterSelectors.includes(target) || childOf( target, 'woof_block_html_items' ) ) 
                return;

            removeHeight( filterSelectors )
        }

        // Show dropdown(s) appriopirately
        filterSelectors.forEach(trigger => {
            trigger.addEventListener('click', () => {
                removeHeight( filterSelectors, trigger )
                
                setTimeout(handleHeight( trigger ), 200)
            })
        })
    }

    initAll( filters, init ) {
        window.ProductsNotFound && 
        window.ProductsNotFound()

        this.setDropdownIcons( filters )
        this.setDropdowns( filters )

        if ( !!init && isLaptop.check ) this.setSideMenu()
    }

    setSideMenu() {
        const navigation = $$('.fuzion-nav.products-filter')
        const openers = [$$('.product-filters__trigger')]

        if ( !navigation ) return;

        new SideMenu({ navigation, openers })
    }
}

// Initialize on load
window.addEventListener('DOMContentLoaded', () => {
    const productSlider = $$('.js-product__side-img')

    if (productSlider) 
        new ImageSwap(productSlider)

    if ($$('.products') && $$('.woof_container'))
        new ProductFilters('.woof_container')
})

// Expose handling nothing
window.ProductsNotFound = () => {
    const $textWrap = $$('#noproducts')
    
    if ( !$textWrap ) return;
    
    const $prodList = $$('ul.products')
    const hasResults = $prodList && $prodList.children.length >= 1

    hasResults 
        ? $textWrap.classList.add('d-none')
        : $textWrap.classList.remove('d-none')
    ;
}

// Expose setup in order for AJAX
window.ProductFilters = ProductFilters