import { BlogFilters } from '../ui/blogFilters'
import $$ from '../lib/_q'

window.addEventListener('DOMContentLoaded', () => {
  const tabJournal = $$(".js-tabs--journal");

  if(tabJournal)
    new BlogFilters(tabJournal)
})