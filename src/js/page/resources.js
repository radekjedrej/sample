import $$ from '../lib/_q'
import { ProductFilters } from './product'
import { MobileFilters } from '../ui/filters'

import InnerSearch from '../lib/innerSearch'
import DeviceCheck from '../lib/deviceCheck'

const isLaptop = new DeviceCheck('laptop')

class Filter {
    constructor({ items, notFound, brochure = [], product = [] }) {
        this.items = items
        this.notFound = notFound
        this.brochure = brochure
        this.product = product
    }

    set(value, type) {
        ! this[type].includes(value)
        ? this[type] = value
        : this[type] = []
    }
}

const Filtered = new Filter({
    items       : $$('.resources-list > a', !0),
    notFound    : $$('#noresources'),
})

function handleChecks( target ) {
    [...target.parentNode.parentNode.children].forEach(li => {
        const $check = li.children[0]

        if ( target !== $check ) $check.checked = false
    })

    const { dataset: { name, type }} = target

    Filtered.set( name, type )
}

function handleContent( searchInputs ) {
    const { items, brochure, product } = Filtered;

    // Handle the case with searchbar
    if ( searchInputs.length ) {
        searchInputs.forEach(el => {
            if ( el.value.length < 1 ) return;

            el.value = '';
            el.nextElementSibling.click()
        })
    }
    
    // Handle filtered results
    [...items]
        .map(el => {
            el.classList.add('-hidden') 
            return el
        })
        .filter(({ dataset }) => {
            if ( brochure.length && product.length )
                return brochure.includes( dataset.brochure ) && product.includes( dataset.product )
            if ( brochure.length )
                return brochure.includes( dataset.brochure ) 
            if ( product.length )
                return product.includes( dataset.product )
                return true
        })
        .forEach(el => el.classList.remove('-hidden'))

    // Handle the case of no results
    const hidden = [...items]
        .filter(el => el.classList.contains('-hidden'))

    hidden.length === [...items].length
        ? Filtered.notFound.classList.remove('d-none')
        : Filtered.notFound.classList.add('d-none')
}

// Initialize on load
window.addEventListener('DOMContentLoaded', () => {
    const $options = $$('.fuzion-filter__checkbox', !0)
    const $searchInputs = $$('.fuzion-filters__search > input', !0)

    const Overlay = new ProductFilters('.fuzion-filters--static .fuzion-options', false)

    if ( isLaptop.check ) {
        new MobileFilters()
    } 
    else {
        Overlay.setSideMenu()
    }

    // Connects the search
    const { items } = Filtered; new InnerSearch({
        items,
        filters: $options
    })

    // Connects the filters
    $options.forEach(el => {
        el.addEventListener('change', () => {
            handleChecks( el )
            handleContent( $searchInputs )
        })
    })
})