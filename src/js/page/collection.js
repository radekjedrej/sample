import $$ from '../lib/_q'

export const Filters = (() => {
    const filters = $$('.collection-filters')

    if ( ! filters ) return;

    const list = filters.nextElementSibling, btns = {
        list: filters.querySelector('[data-toggle=list]'),
        grid: filters.querySelector('[data-toggle=grid]'),
    }

    let isGrid = false

    for ( const btn of Object.keys(btns) ) 
        btns[btn].onclick = () => {
            isGrid = !isGrid 
            handleButtons()
        }

    const handleButtons = () => {
        list.setAttribute('data-view', isGrid ? 'grid' : 'list')

        if ( isGrid ) {
            btns.list.classList.remove('-active')
            btns.grid.classList.add('-active')
        } 
        else {
            btns.list.classList.add('-active')
            btns.grid.classList.remove('-active')
        }
        
    }

})()