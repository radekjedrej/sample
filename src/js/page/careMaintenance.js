import $$ from '../lib/_q'
import { MobileFilters } from '../ui/filters'
import { Tabs } from '../lib/_c'


window.addEventListener('DOMContentLoaded', () => {
  const tab = $$(".js-tabs--desktop");
  new MobileFilters();
  new Tabs(tab);

  const pageURL = encodeURI(document.location.href);
  const pageProfessional = pageURL.includes("professional.fuzionflooring")

  if ( !pageProfessional ) return

  const supportLinks = $$(".site-links__item", !0)
  let supportLink = supportLinks[supportLinks.length - 1];
  var res = supportLink.href.replace("contact-us", "contact-us-professional");
  supportLink.href = res
})
