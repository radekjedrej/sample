export class SocialMedia {
  constructor() {
    this.postTitle = encodeURI(document.querySelector(".js-banner-title").innerHTML)
    this.postUrl = encodeURI(document.location.href);
    this.facebookBtn = document.querySelector(".facebook-btn a")
    this.twitterBtn = document.querySelector(".twitter-btn a")
    this.linkedinBtn = document.querySelector(".linkedin-btn a")
    this.emailBtn = document.querySelector(".email-btn")

  
    this.init()
  }

  init() {

    this.facebookBtn.setAttribute(
      "href",
      `https://www.facebook.com/sharer.php?u=${this.postUrl}`
    );
  
    this.twitterBtn.setAttribute(
      "href",
      `https://twitter.com/share?url=${this.postUrl}&text=${this.postTitle}`
    );
  
    this.linkedinBtn.setAttribute(
      "href",
      `https://www.linkedin.com/shareArticle?url=${this.postUrl}&title=${this.postTitle}`
    );
  
    this.emailBtn.setAttribute(
      "href",
      `mailto:?subject=${this.postTitle}&body=${this.postUrl}`
    );
  }
}

const socialMedia = new SocialMedia()