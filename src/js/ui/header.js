import $$ from '../lib/_q'
import { handleHeight } from '../lib/_c'
import SideMenu from '../lib/sideMenu'

const $nav = $$('.fuzion-nav')
const $navBack = $$('.fuzion-nav .btn--back')
const $dropdown = $$('.fuzion-header .fuzion-dropdown')

const Drawer = (() => {
    const triggers = $$('.site-links__item--draw', !0)

    if ( ! triggers.length ) return;

    triggers.forEach((trigger) => {
        trigger.addEventListener('click', function(event) {
            event.preventDefault()
            
            handleHeight( this )
        })
    })

})()

const Lang = (() => {
    const trigger = $$('.fuzion-header .fuzion-langs .-current')

    if ( ! trigger ) return;

    trigger.addEventListener('click', function(event) {
        event.preventDefault()
        
        handleHeight( this )
    })
})()

const Cart = (() => {
    const triggers = $$('.add_to_cart_button', !0)
    const $button = $$('#cart-items')
    const $icon = $$('[aria-label=Cart]')

    if ( ! $icon || ! triggers.length ) return;

    $icon.onclick = () => {
        location.assign('/cart')
    }

    let counter = parseInt($button.innerHTML)

    triggers.forEach(el => el.addEventListener('click', () => 
        setTimeout(() => {
            $button.innerHTML = (counter++)
        }, 350)
    ))
})()

const Sticky = ((className = '-sticky') => {
    const $head = $$('.fuzion-header')
    const $hero = $$('.section-white.slider--full')

    if ( ! $hero ) return; const { clientHeight } = $hero

    window.addEventListener('scroll', function() {
        requestAnimationFrame(() => {
            if ( this.scrollY > clientHeight - $head.clientHeight ) {
                $head.classList.contains(className) ||
                $head.classList.add(className)
            } else {
                $head.classList.contains(className) && 
                $head.classList.remove(className)
            }
        })
    })
})()

const Header = ((speed = 250) => {

    $nav.classList.add('-ready')

    class Menu extends SideMenu {

        constructor({ navigation, openers }) {
            super({
                navigation,
                openers,
            })

            super.init()
        }

        submenus( event ) {
            const { 
                target, 
                target: { nextElementSibling: next }
            } = event

            if ( next.children.length ) {
                event.preventDefault();

                [...target.parentNode.parentNode.children].forEach(
                    el => el.classList.remove('-visible')
                )
                target.parentNode.classList.add('-visible')
                $navBack.classList.remove('-hidden')
            }
        }

        submenusClose({ target }) {
            target.classList.add('-hidden');

            [...target.nextElementSibling.children].forEach(
                el => el.classList.remove('-visible')
            )
        }

        init() {
            const $navLinks = $$('.fuzion-menu.fuzion-menu--overlay > li > a', !0)

            $navBack.addEventListener('click', this.submenusClose)
            $navLinks.forEach(el => el.addEventListener('click', this.submenus))
        }
    }

    class Dropdown extends SideMenu {

        constructor({ navigation, openers, dropdowns }) {
            super({
                navigation,
                openers,
            })

            this.dropdowns = dropdowns

            super.init()
        }

        openMenu( x ) {
            super.openMenu()

            // Hidden all dropdowns
            const current = Array.from(this.dropdowns)
                .map((el) => { el.hidden = true; return el })
                .filter((el, i) => i === x && (el))
                .pop() 
            // Except current
            current.removeAttribute('hidden')

            const { clientHeight } = this.dropdowns[x]

            setTimeout(() => {
                this.navigation.style.height = `${clientHeight}px`
            }, super.speed)
        }

        closeMenu() {
            super.closeMenu()
            
            setTimeout(() => {
                this.navigation.style.height = '0px'
            }, super.speed)
        }

        hovers( time, x, collections, descriptions ) {
            [...collections, ...descriptions].forEach((el, x, d) => {
                if ( d = el.parentNode.classList ) 
                    d.contains('-animate') && d.remove('-animate')
                    el.hidden = true
            })
            
            time = setTimeout(() => {
                collections[x].hidden = null 
                descriptions[x].hidden = null 
                descriptions[x].parentNode.classList.add('-animate')
            }, 0)
        }

        init() {
            const triggers = $$('.fuzion-dropdown .fuzion-submenu a', !0)
            const collections = $$('.fuzion-dropdown .fuzion-collection a', !0)
            const descriptions = $$('.fuzion-dropdown .js-dropdown-description', !0)

            // Handle dropdowns hovers
            let time = null; triggers.forEach((el, x) => {

                const { description } = el.dataset
                descriptions[x].innerHTML = description
                
                el.addEventListener('mouseover', () => {
                    this.hovers( time, x, collections, descriptions )
                }, false)
            })

        }
    }

    return {
        Menu: new Menu({
            navigation: $$('.fuzion-nav'),
            openers: [$$('.fuzion-header .btn--menu')],
        }),
        Dropdown: new Dropdown({
            navigation: $dropdown,
            dropdowns: $dropdown.querySelectorAll('.fuzion-dropdown__wrap'),
            openers: $$('.header-nav .fuzion-menu a', !0),
        }),
    }

})

export class Menus {

    constructor() {
        this.initAll()
    }

    initAll() {
    if ( $dropdown ) {
        const FuzionMenus = new Header()
        // console.log( FuzionMenus.Menu )
    }
    }
    
}