import $$ from '../lib/_q'

export class BlogFilters {
  constructor(tab) {
    this.tab = tab
    this.tabs = $$('.tab-button', !0)
    this.contents = $$('.journal__list', !0)
    this.onLoadContent = $$('.tab-button.open input').dataset.filter

    this.init()
  }

  showContent(e) {
    const { target: { dataset }} = e
    const { filter } = dataset

    if ( ! filter ) return;

    this.contents.forEach(content => content.classList.remove("journal__list--active"));
    this.tabs.forEach(btn => btn.classList.remove("open"));

    const contentsActive = Array.from(this.contents).filter(el => el.dataset.category.includes(filter))
    contentsActive.forEach( el => el.classList.add("journal__list--active"));
  }

  initShowContent() {
    const contentsActive = Array.from(this.contents).filter(el => el.dataset.category.includes(this.onLoadContent))
    contentsActive.forEach( el => el.classList.add("journal__list--active"));
  }

  init() {
    this.initShowContent()
    this.tab.onclick = (e) => this.showContent(e)
  }
}