import { tns } from "tiny-slider/src/tiny-slider"
import $$ from '../lib/_q'

const _move = (x, links, thumbs) => {
    const percent = 100 * -x;

    [...links.children, ...thumbs.children].forEach(el => {
        el.style.transform = `translate3d(${percent}%, 0, 0)`
    })
}

const _forEach = function (array, callback, scope) {
    for (let i = 0; i < array.length; i++) {
        callback.call(scope, i, array[i])
    }
}

export const Sliders = ((controls = null) => {

    const $tinySliders = $$('.slide-wrap', !0)
    const $doubleSlides = $$('.slider--double', !0)

    const defaults = {
        mouseDrag   : true,
        items       : 1,
        speed       : 1000,
        slideBy     : 1, //'page',
        navPosition : 'bottom',
        autoplayPosition: 'bottom',
    }

    // Handle tiny sliders
    _forEach($tinySliders, function(x, wrapper) {
        const { slider } = wrapper.dataset

        let settings = {}

        switch ( slider ) {
            case 'home': 
                settings.autoplay = true
            break
            case 'gallery':
                settings.controls = false
                settings.lazyload = true
                settings.items = 1
                settings.center = true
                settings.gutter = 100
                settings.responsive = {
                    767: {
                        controls: true,
                        items: 2
                    }
                }
            break
            case 'product':
                settings.controls = false
                settings.gutter = 35
                settings.disable = false
                settings.responsive = {
                    767: {
                        disable: true,
                    }
                }
            break
        }

        if ( controls = wrapper.parentNode.querySelector('.slide-nav') )
            settings.controlsContainer = controls
            settings.container = wrapper

        const run = tns({
            ...defaults,
            ...settings,
        })

    })

    // Handle custom slider
    _forEach($doubleSlides, function(x, el) {

        const $customSlides = el.querySelectorAll('.site-links a')
        const $links =  el.querySelector('[data-slider=slideImage]')
        const $thumbs =  el.querySelector('[data-slider=slideText]')

        if ( !$links || !$thumbs ) return;

        _forEach($customSlides, function(x, el) {
        
            // Handle `active` onload
            x === 0 && el.classList.add('-active')
            
            // Watch `active` onlink
            el.addEventListener('click', (event) => {
                    event.preventDefault()
    
                    $customSlides.forEach(({ classList }) => classList.remove('-active'))
                    
                    el.classList.add('-active')
    
                    requestAnimationFrame( 
                        () => _move( x, $links, $thumbs )
                    )
            })
    
        })
    })

    const PositionDots = (() => {
        const $tinySliders = document.querySelectorAll('.tns-outer')
    
        $tinySliders.forEach((slider) => {
            
            const dots = slider.querySelector('.tns-nav')
            const slideInfo = slider.querySelector('.slide__info')
    
            if ( ! slideInfo ) return;
    
            const textHeight = slideInfo.offsetHeight + 20
            dots.setAttribute('style', `bottom: ${textHeight}px`)
        })
        
    })()

})()