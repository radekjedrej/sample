import SideMenu from '../lib/sideMenu'

export default class Search {
  constructor(searchButton, formPopup) {
    this.searchButton = searchButton
    this.formPopup = formPopup

    this.init()
  }

  init() {

    new SideMenu({ 
      navigation: this.formPopup, 
      openers: [this.searchButton],
      closers: [],
      speed: 200,
    })

  }
}
