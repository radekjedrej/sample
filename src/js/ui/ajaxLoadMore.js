import axios from 'axios';

export const AjaxLoadMore = ((button) => {

  button.addEventListener('click', (e) => {
    e.preventDefault();

    button.classList.add("btn--inactive");

    let current_page = document.querySelector('.projects').dataset.page;
    let max_pages = document.querySelector('.projects').dataset.max;

    let params = new URLSearchParams();
    params.append('action', 'load_more_posts');
    params.append('current_page', current_page);
    params.append('max_pages', max_pages);

    axios.post('/wp-admin/admin-ajax.php', params)
      .then(res => {

        let posts_list = document.querySelector('.projects');

        posts_list.insertAdjacentHTML('beforeend', res.data.data);

        let getUrl = window.location;
        let baseUrl = getUrl.protocol + "//" + getUrl.host + "/";

        window.history.pushState('', '', baseUrl + 'projects/?page-' + (parseInt(document.querySelector('.projects').dataset.page) + 1));

        document.querySelector('.projects').dataset.page++;

        if (document.querySelector('.projects').dataset.page == document.querySelector('.projects').dataset.max) {
          button.parentNode.removeChild(button);
        } else {
          button.classList.remove("btn--inactive");
        }

        let all_posts = document.querySelectorAll('.projects__box')
        let count = 1

        all_posts.forEach((item, index) => {

          item.className  = `projects__box projects__box--${count}`
          count++
          count === 5 ? count = 1 : count
        });

      })
  })
  
})