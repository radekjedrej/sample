import $$ from '../lib/_q'
import { Tabs } from '../lib/_c'
import SideMenu from '../lib/sideMenu'
import DeviceCheck from '../lib/deviceCheck'

const isMobile = new DeviceCheck('mobile')
const isLaptop = new DeviceCheck('laptop')

export class MobileFilters {

    constructor() {
        const navigation = $$('.fuzion-nav.fuzion-filter')
        const openers = [$$('.js-tabs-filter')]
        
        if ( !isLaptop.check || !navigation ) return;

        this.setTabs({ navigation })
        this.setOverlay({ navigation, openers })
    }

    setTabs({ navigation }) {
        const tabsUl = $$('.js-tabs_ul');
        const menuUl = navigation.querySelector('.js-fuzion-filter__list');

        [...tabsUl.children].forEach(element => {
            menuUl.insertAdjacentElement('beforeEnd', element);
        });

        new Tabs($$(".js-tabs--mobile"))
    }

    setOverlay( el ) {

        new SideMenu(el)
    }
    
}