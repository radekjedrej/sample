import $$ from './_q'

export default class InnerSearch {
    constructor({ el, items, filters }) {
        this.el = el || $$('.fuzion-filters__search', !0)
        this.items = items
        this.filters = filters

        if ( !this.el ) return;

        [...this.el].forEach(el => this.initAll(el))
    }

    initAll(form) {
        const [ input, button ] = form.children

        input.parentNode.onsubmit = event => event.preventDefault()

        input.addEventListener('input', 
            ({ target }) => resetAndSearch.call(this, target)
        )
    
        button.addEventListener('click', 
            event => toggleSearch.call(this, event)
        )
    }
}

function resetAndSearch({ value }) {

    [...this.filters]
        .forEach(el => el.checked = false)
    ;

    [...this.items]
        .map(el => {
            el.classList.add('-hidden') 
            return el
        })
        .filter(el => {
            const { name } = el.dataset
            return value
                ? name.includes( value.toLowerCase() )
                : true
        })
        .forEach(el => el.classList.remove('-hidden'))
    ;
}

function toggleSearch( event ) {
    const { parentNode: parent } = event.target
    const [ input ] = parent 

    event.preventDefault()

    if ( parent.classList.contains('-active') ) {
        parent.classList.remove('-active')
        input.blur()
    } 
    else {
        parent.classList.add('-active')
        input.focus()
    }
} 
    