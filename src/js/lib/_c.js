// Handles parent-root checking
export const childOf = (el, className = 'woof_block_html_items', parent) => {
    const { classList } = el.parentNode

    if ( classList && classList.contains(className) )
        return true

    else if (( parent = el.parentNode) && parent.classList )
        return childOf( parent ) // Make sure that any parent has a className
        return false // Otherwise don't have parents
}

// Handles auto-height transition
export const handleHeight = ( btn ) => {
    const el = btn.nextElementSibling
    const height = el.scrollHeight

    requestAnimationFrame(() => {
        btn.classList.toggle('active')
        el.style.height = !el.style.height
            ? `${height}px`
            : null
    })
}

// Templates :: 
export const Accordion = (() => {

  function toggleAccordion(x) {
    const button = document.querySelectorAll('.accordion__question')

    for (let i = 0; i < button.length; i++) {
      button[i].addEventListener("click", function() {
        /* Toggle between hiding and showing the active panel */
        handleHeight( this )
      });
    }
  }

  var x = window.matchMedia("(max-width: 767px)")
  toggleAccordion(x) // Call listener function at run time
    
})

// Templates :: 
export const Tabs = ((tab) => {

    const tabs = document.querySelectorAll(".tab-button")
    const contents = document.querySelectorAll(".tab__content")
    
    tab.onclick = (e) => {
        const { target, target: { dataset }} = e
        const { filter } = dataset

        if ( ! filter ) return;

        tabs
            .forEach(btn => btn.classList.remove("open"));

        contents
            .forEach(el => el.classList.remove("open"));

        target
            .parentElement.classList.add("open");

        const element = document.getElementById(filter);
        element.classList.add("open");

    }
})

// Templates :: 
// (Home) Front Page, Project Single
export const BgChange = ((component) => {
  
    const isDark = component.classList.contains('quote--dark');

    if ( !isDark ) return;

    component.classList.remove('quote--dark')

    function checkViewport() {
        const { offsetTop, offsetHeight } = component
        const { scrollY, innerHeight } = window

        // third way through the image
        const changeBgAt = (scrollY + innerHeight) - offsetHeight / 3;

        const isShown = changeBgAt > offsetTop;
        const isNotScrolled = scrollY < offsetTop + offsetHeight;

        if ( isShown && isNotScrolled ) {
            component.classList.add('quote--dark');
        } else {
            component.classList.remove('quote--dark');
        }
    }

    window.addEventListener('scroll', function() {
        requestAnimationFrame(() => {
            checkViewport()
        })
    });
})

export const ToggleClass = (element, className) => {
    className.forEach(el => {
        element.classList.add(el)
    })
}
