import $$ from '../lib/_q'

export default class SideMenu {
    constructor({ openers, closers, navigation,
        over = $$('.fuzion-overlay'), 
        speed = 250 
    }) {
        
        this.navigation = navigation
        this.openers = openers
        this.closers = closers 
            ? closers 
            : this.preset()
        this.over = over
        this.speed = speed
        this.active = false
        this.input = $$('.js-search-input')

        this.init()
    }

    openMenu() {
        this.active = true        
        this.over.classList.add('-visible')

        setTimeout(() => {
            this.navigation.classList.add('-visible')
            this.navigation.removeAttribute('aria-hidden')
        }, this.speed)

        if ( ! this.input ) return;
        setTimeout(() => this.input.focus(), 301)
    }

    closeMenu() {
        this.active = false
        this.navigation.classList.remove('-visible')
        this.navigation.setAttribute('aria-hidden', !0)
        
        setTimeout(() => {
            this.over.classList.remove('-visible')
        }, this.speed)
    }

    preset() {
        if ( ! this.navigation ) return [];

        const { children: els } = this.navigation

        return [els[0].localName == 'button'
            ? els[0] : (els[1] || null)
        ];
    }

    init() {   
        const that = this; this.navigation.classList.add('-ready')
        
        const hasDropdown = el => el.classList.contains('fuzion-menu__trigger')
        const getIndex = ({ parentElement: p }) => [...p.parentElement.children].indexOf(p)

        // Enable opening on triggers
        this.openers.forEach(function(el) {
            el && el.addEventListener('click', function(e) {
                hasDropdown(el) && e.preventDefault();

                // Toggle, but not dropdowns
                hasDropdown(el) || !that.active
                    ? that.openMenu( getIndex(el) )
                    : that.closeMenu()
            })
        });

        // Enable closing on overlay and/or triggers
        [this.over, ...this.closers].forEach(function(el) {
            el && el.addEventListener('click', function() {
                that.active && that.closeMenu()
            })
        });
    }
} 