export default function( btn ) {
    const el = btn.nextElementSibling
    const height = el.scrollHeight

    requestAnimationFrame(() => {
        btn.classList.toggle('active')
        el.style.height = !el.style.height
            ? `${height}px`
            : null
    })
}