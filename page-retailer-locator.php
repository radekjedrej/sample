<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package storefront
 */

get_header();

    while ( have_posts() ) :
        the_post(); ?>

        <div class="retailer" data-grid=head>

            <?php get_template_part( 'content', 'page' ); ?>

        </div>

        <?php
        
    endwhile; 
    
do_action( 'storefront_sidebar' );
get_footer();
