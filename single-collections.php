<?php
/**
 * The template for displaying all single posts.
 *
 * @package storefront
 */

$dark_header = true;

get_header(); ?>

	<main data-grid=top><?php

		$collectionSlug = get_field('category_slug');
		$category = get_term_by( 'slug', $collectionSlug, 'product_cat' );
		$category_link = get_category_link( $category->term_id );

		set_query_var('banner_copy', array(
			'title' => get_the_title() . ' ' . __('Collections', 'fuzion'),
			'cta_href' => $category_link,
			'cta_text' => __('View all Products', 'fuzion')
		));
		get_template_part('parts/banner', 'inner') ?>

		<div class="collection-filters">
			<button aria-label="<?= __('Toggle List View', 'fuzion') ?>" data-toggle="list" class="btn--clean -active">
				<?php include get_icons_directory('v-list.svg') ?>
			</button>
			<button aria-label="<?= __('Toggle Grid View', 'fuzion') ?>" data-toggle="grid" class="btn--clean">
				<?php include get_icons_directory('v-grid.svg') ?>
			</button>
		</div>
		
		<?php 
		$cat = get_term_by( 'slug', get_field('category_slug'), 'product_cat' );
		$subargs = array(
			'hierarchical' => 1,
			'show_option_none' => '',
			'hide_empty' => 0,
			'parent' => $cat->term_id,
			'taxonomy' => 'product_cat'
		);
	  	$subcats = get_categories($subargs);
		
		echo '<ul class="collection-list list list--block" data-view=list>';
		foreach ($subcats as $sc) : $link = get_term_link( $sc->slug, $sc->taxonomy ) ?>
		<li class="collection">
			<a href="<?= $link ?>" class="chapter chapter--cols chapter--end" data-flex="row justify">
				<div class="collection__heading chapter__heading"><h2 class="section-title"><?= $sc->name; ?></h2></div>
				<figure class="collection__figure" data-flex="row justify">
					<p><?= $sc->description ?></p>
					<?php $thumbnail_id = get_term_meta( $sc->term_id, 'thumbnail_id', true ); ?>
					<?= wp_get_attachment_image( $thumbnail_id, 'medium' ) ?>
				</figure>
			</a>
		</li>
		<?php
		endforeach;
	    echo '</ul>';
		?>

	</main>

<?php
do_action( 'storefront_sidebar' );
get_footer();